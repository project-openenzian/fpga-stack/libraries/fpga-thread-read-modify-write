# fpga_thread_rmw


Modules for FPGA threads to read modify write data
into the FPGA memory.

## Common_for_sim
This folder contains common modules that will be picked
from the DC. They are here only for simulation.

## ft_cnc_rmw.sv
FPGA threads issue a read request with ID and address.
The module stores the address in a memory indexed by
the ID and marks this ID as busy (subsequent reads
to same ID would cause head of line blocking).

The module than issues an LCI to the DC tagged with
the same ID. When LCIA arrives, the CL is guaratneed
to be invalid in CPU cache and FPGA memory has most
up-to-date value. This LCIA also triggers the module
to read the FPGA memory (mem trnscn tagged with same ID).

When read response arrives, the ID and data
are sent to the FPGA thread. The thread can now
modify the data and write it back with the
same ID. (Note MUST use same ID).

The module then uses the ID from the write
request to identify the address being written to
and performs the write request with the mem.
The same ID is also used to tag the wr trnscn.

when memory responds to the write request,
the module uses the ID again to identify the
address and send an unlock message.
There are no responses to FPGA threads indicating
the write has completed. 

## ft_dc_desc_arb.sv
Arbitrate between read and write
requests between DC and FPGA threads
to issue one request to the memory.

Responses from memory are routed to
either DC or FPGA thread using the ID.

FPGA IDs are 5 bits, and DC IDs are 7 bits.
with DS_NUM_SETS_PER_DCU == 128, we have
MSB of ID issued from DC to be always 0.
we convert the 5 bit FPGA thread ID into 7 bits
with MSB set to 1 and rest to 0 before
sending requests.

when responses arrive the MSB is used to
route to DC or FPGA threads.
if MSB of response ID is 1, route to FPGA
threads else route to DC. 

## f_rmw_thread.sv
This module generates specific number of 
sequential aliased odd or even addresses
from a start address and issues a read
request for the addresses.

when the read response arrives, the data
is modified and written back. 

Timer keeps track of latency of operation
from start to finish.

