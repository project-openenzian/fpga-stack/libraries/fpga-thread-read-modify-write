`ifndef WORD_ADDR_MEM_SV
`define WORD_ADDR_MEM_SV

// Intended to be used only for simulation. Synthesis will create a distributed RAM.

// Simple dual port RAM (distributed RAM) 
// Writing is always full CLs.
// Read first mode, when writing and reading to same address. The read output contains
// the previous value that was stored in the RAM. 

module word_addr_mem #
  (
   parameter ID_WIDTH = 6,
   parameter ADDR_WIDTH = 10,
   parameter DATA_WIDTH = 1024
   )
   (
    input logic 		  clk,
    input logic 		  reset,

    // Read request. 
    input logic [ID_WIDTH-1:0] 	  rd_req_id_i,
    input logic [ADDR_WIDTH-1:0]  rd_req_addr_i,
    input logic 		  rd_req_valid_i,
    output logic 		  rd_req_ready_o,

    // Read response.
    output logic [ID_WIDTH-1:0]   rd_rsp_id_o,
    output logic [DATA_WIDTH-1:0] rd_rsp_data_o,
    output logic 		  rd_rsp_valid_o,
    input logic 		  rd_rsp_ready_i,

    // write request.
    input logic [ID_WIDTH-1:0] 	  wr_req_id_i,
    input logic [ADDR_WIDTH-1:0]  wr_req_addr_i,
    input logic [DATA_WIDTH-1:0]  wr_req_data_i,
    input logic 		  wr_req_valid_i,
    output logic 		  wr_req_ready_o,

    // write response.
    output logic [ID_WIDTH-1:0]   wr_rsp_id_o,
    output logic [1:0] 		  wr_rsp_bresp_o,
    output logic 		  wr_rsp_valid_o,
    input logic 		  wr_rsp_ready_i

    );

   localparam RAM_DEPTH = 2**ADDR_WIDTH;

   // RAM memory. 
   logic [DATA_WIDTH-1:0] 	  ram_mem [RAM_DEPTH-1:0];

   // RAM write port signals.
   logic 			  rwp_en, rwp_we;
   logic [ADDR_WIDTH-1:0] 	  rwp_addr;
   logic [DATA_WIDTH-1:0] 	  rwp_data;
   
   // RAM read port signals.
   logic 			  rrp_en;
   logic [ADDR_WIDTH-1:0] 	  rrp_addr;
   logic [DATA_WIDTH-1:0] 	  rrp_data;

   logic [ID_WIDTH-1:0] 	  rd_rsp_id_reg = '0, rd_rsp_id_next;
   logic [ID_WIDTH-1:0] 	  wr_rsp_id_reg = '0, wr_rsp_id_next;
   
   // state machine signals. 
   enum {RD_INIT, RD_REQ, RD_RSP} rd_state_reg, rd_state_next;
   enum {WR_INIT, WR_REQ, WR_RSP} wr_state_reg, wr_state_next;

   //Non synthesizable
   integer i;
   initial begin
      for(i = 0; i< RAM_DEPTH; i++) begin
	 ram_mem[i] = DATA_WIDTH'(i);
      end
   end

   
   // Output signal assignment.
   assign rd_rsp_id_o = rd_rsp_id_reg;
   assign rd_rsp_data_o = rrp_data;
   assign wr_rsp_id_o = wr_rsp_id_reg;
   assign wr_rsp_bresp_o = '0;

   // RAM signal assignments.
   // Simple dual port RAM (distributed).
   assign rrp_addr = rd_req_addr_i;
   assign rwp_data = wr_req_data_i;
   assign rwp_addr = wr_req_addr_i;
   
   // Ram write port. 
   always_ff @(posedge clk) begin : RAM_SDP_WRITE_PORT
      if(rwp_en) begin
	 if(rwp_we) begin
	    ram_mem[rwp_addr] <= rwp_data;
	 end
      end
   end : RAM_SDP_WRITE_PORT

   // RAM read port.
   always_ff @(posedge clk) begin : RAM_SDP_READ_PORT
      if(rrp_en) begin
	 rrp_data <= ram_mem[rrp_addr];
      end
   end : RAM_SDP_READ_PORT

   always_comb begin : RD_CONTROLLER
      rd_state_next = rd_state_reg;
      rd_rsp_id_next = rd_rsp_id_reg;
      rd_req_ready_o = 1'b0;
      rd_rsp_valid_o = 1'b0;
      rrp_en = 1'b0;

      // Upon reset go to RD_INIT state for 1 cycle then goto RD_REQ state.
      // In RD_REQ state, the module is ready to accept new read requests.
      // when handshake happens in read req channel, the read port of RAM is enabled.
      // and data from RAM will be latched.
      // In RD_RSP state, the read response is valid and waits for handshake in read
      // response interface before continuing to accept new requests. 
      
      case(rd_state_reg)
	RD_INIT: begin
	   rd_state_next = RD_REQ;
	end
	RD_REQ: begin
	   rd_req_ready_o = 1'b1;
	   if(rd_req_ready_o & rd_req_valid_i) begin
	      rrp_en = 1'b1;
	      rd_rsp_id_next = rd_req_id_i;
	      rd_state_next = RD_RSP;
	   end
	end
	RD_RSP: begin
	   rd_rsp_valid_o = 1'b1;
	   if(rd_rsp_valid_o & rd_rsp_ready_i) begin
	      rd_state_next = RD_REQ;
	   end
	end
      endcase
   end : RD_CONTROLLER

   always_comb begin : WR_CONTROLLER
      wr_state_next = wr_state_reg;
      wr_rsp_id_next = wr_rsp_id_reg;
      wr_req_ready_o = 1'b0;
      wr_rsp_valid_o = 1'b0;
      rwp_en = 1'b0;
      rwp_we = 1'b0;
      
      case(wr_state_reg)
	WR_INIT: begin
	   wr_state_next = WR_REQ;
	end
	WR_REQ: begin
	   // ready to accept new write requests.
	   // when write handshake happens, enable write port of SDP RAM. 
	   wr_req_ready_o = 1'b1;
	   if(wr_req_ready_o & wr_req_valid_i) begin
	      wr_rsp_id_next = wr_req_id_i;
	      rwp_en = 1'b1;
	      rwp_we = 1'b1;
	      wr_state_next = WR_RSP;
	   end
	end
	WR_RSP: begin
	   wr_rsp_valid_o = 1'b1;
	   if(wr_rsp_valid_o & wr_rsp_ready_i) begin
	      wr_state_next = WR_REQ;
	   end
	end
      endcase
      
   end : WR_CONTROLLER

   always_ff @(posedge clk) begin : REG_ASSIGN
      rd_state_reg <= rd_state_next;
      wr_state_reg <= wr_state_next;
      rd_rsp_id_reg <= rd_rsp_id_next;
      wr_rsp_id_reg <= wr_rsp_id_next;
      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset 
	 rd_state_reg <= RD_INIT;
	 wr_state_reg <= WR_INIT;
	 rd_rsp_id_reg <= '0;
	 wr_rsp_id_reg <= '0;
      end
   end : REG_ASSIGN   
   
endmodule // word_addr_mem

`endif
