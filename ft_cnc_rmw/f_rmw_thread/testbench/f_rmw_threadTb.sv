import eci_cmd_defs::*;
import eci_dcs_defs::*;

module f_rmw_threadTb();

   parameter logic ODD_GEN = 1'b0;

   //input output ports 
   //Input signals
   logic 	   clk;
   logic 	   reset;
   logic [DS_ADDR_WIDTH-1:0] st_un_al_addr_i;
   logic [DS_ADDR_WIDTH-1:0] num_cls_to_rmw_i;
   logic 		     rmw_en_i;
   logic 		     f_rd_req_ready_i;
   logic [5-1:0] 	     f_rd_rsp_id_i;
   logic [ECI_CL_WIDTH-1:0]  f_rd_rsp_data_i;
   logic 		     f_rd_rsp_valid_i;
   logic 		     f_wr_ready_i;

   //Output signals
   logic 		     rmw_done_o;
   logic [5-1:0] 	     f_rd_req_id_o;
   logic [DS_ADDR_WIDTH-1:0] f_rd_req_al_addr_o;
   logic 		     f_rd_req_valid_o;
   logic 		     f_rd_rsp_ready_o;
   logic [5-1:0] 	     f_wr_id_o;
   logic [ECI_CL_WIDTH-1:0]  f_wr_data_o;
   logic 		     f_wr_valid_o;
   logic [DS_ADDR_WIDTH-1:0] tot_timer_o;
   logic [DS_ADDR_WIDTH-1:0] num_wr_issued_o;
   logic [DS_ADDR_WIDTH-1:0] num_rd_issued_o;
   


   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   f_rmw_thread #
     (
      .ODD_GEN(ODD_GEN)
      )
   f_rmw_thread1 (
		  .clk(clk),
		  .reset(reset),
		  .st_un_al_addr_i(st_un_al_addr_i),
		  .num_cls_to_rmw_i(num_cls_to_rmw_i),
		  .rmw_en_i(rmw_en_i),
		  .f_rd_req_ready_i(f_rd_req_ready_i),
		  .f_rd_rsp_id_i(f_rd_rsp_id_i),
		  .f_rd_rsp_data_i(f_rd_rsp_data_i),
		  .f_rd_rsp_valid_i(f_rd_rsp_valid_i),
		  .f_wr_ready_i(f_wr_ready_i),
		  .rmw_done_o(rmw_done_o),
		  .f_rd_req_id_o(f_rd_req_id_o),
		  .f_rd_req_al_addr_o(f_rd_req_al_addr_o),
		  .f_rd_req_valid_o(f_rd_req_valid_o),
		  .f_rd_rsp_ready_o(f_rd_rsp_ready_o),
		  .f_wr_id_o(f_wr_id_o),
		  .f_wr_data_o(f_wr_data_o),
		  .f_wr_valid_o(f_wr_valid_o),
		  .tot_timer_o(tot_timer_o),
		  .num_rd_issued_o(num_rd_issued_o),
		  .num_wr_issued_o(num_wr_issued_o)
		  );//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports 
      clk = '0;
      reset = '1;
      st_un_al_addr_i = '0;
      num_cls_to_rmw_i = '0;
      rmw_en_i = '0;
      f_wr_ready_i = '0;
      
      ##5;
      reset = 1'b0;
      f_wr_ready_i = 1'b1;
      ##1;
      st_un_al_addr_i = '0;
      num_cls_to_rmw_i = 'd10;
      rmw_en_i = 1'b1;
      // runs till rmw_en_i = 1'b0
      ##100;
      rmw_en_i = 1'b0;
      wait(rmw_done_o);
      ##1;

      ##5;
      st_un_al_addr_i = '0;
      num_cls_to_rmw_i = 'd5;
      rmw_en_i = 1'b1;
      ##100;
      rmw_en_i = 1'b0;
      wait(rmw_done_o);
      ##1;

      #500 $finish;
   end

   always_comb begin : RD_RSP_LOOPBACK
      f_rd_rsp_id_i = f_rd_req_id_o;
      f_rd_rsp_data_i = '1;
      f_rd_rsp_valid_i = f_rd_req_valid_o;
      f_rd_req_ready_i = f_rd_rsp_ready_o;
   end : RD_RSP_LOOPBACK

endmodule
