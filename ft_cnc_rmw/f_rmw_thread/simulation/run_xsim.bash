#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
      $XILINX_VIVADO/data/verilog/src/glbl.v \
      ../rtl/eci_cmd_defs.sv \
      ../rtl/eci_dcs_defs.sv \
      ../testbench/f_rmw_threadTb.sv \
      ../rtl/f_rmw_thread.sv \
      ../rtl/gen_seq_al.sv


xelab -debug typical -incremental -L xpm worklib.f_rmw_threadTb worklib.glbl -s worklib.f_rmw_threadTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.f_rmw_threadTb 
xsim -gui worklib.f_rmw_threadTb 
