/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2023-02-27
 * Project : Enzian
 *
 * Copyright (c) 2023, ETH Zurich.  All rights reserved.
 *
 */

`ifndef F_RMW_THREAD_SV
`define F_RMW_THREAD_SV

import eci_cmd_defs::*;
import eci_dcs_defs::*;


// Read modify write thread.
//
// This module generates specific number of 
// sequential aliased odd or even addresses
// from a start address and issues a read
// request for the addresses.
//
// when the read response arrives, the data
// is modified and written back. 
//
// Timer keeps track of latency of operation
// from start to finish.
//
// When enabled, this module runs continuously
// until disabled. re-enable the module only
// after done is asserted.

module f_rmw_thread #
  (
   // If 1 generate odd aliased addresses, else
   // generate even aliased addresses. 
   parameter logic ODD_GEN = 1'b1
   )
   (
    input logic 		     clk,
    input logic 		     reset,

    input logic [DS_ADDR_WIDTH-1:0]  st_un_al_addr_i,
    input logic [DS_ADDR_WIDTH-1:0]  num_cls_to_rmw_i,
    input logic 		     rmw_en_i,
    output logic 		     rmw_done_o,

    // Output read request. 
    output logic [5-1:0] 	     f_rd_req_id_o,
    output logic [DS_ADDR_WIDTH-1:0] f_rd_req_al_addr_o,
    output logic 		     f_rd_req_valid_o,
    input logic 		     f_rd_req_ready_i,

    // Input read response. 
    input logic [5-1:0] 	     f_rd_rsp_id_i,
    input logic [ECI_CL_WIDTH-1:0]   f_rd_rsp_data_i,
    input logic 		     f_rd_rsp_valid_i,
    output logic 		     f_rd_rsp_ready_o,

    // Output write request.
    output logic [5-1:0] 	     f_wr_id_o,
    output logic [ECI_CL_WIDTH-1:0]  f_wr_data_o,
    output logic 		     f_wr_valid_o,
    input logic 		     f_wr_ready_i,

    // Output performance counters. 
    output logic [DS_ADDR_WIDTH-1:0] tot_timer_o,
    output logic [DS_ADDR_WIDTH-1:0] num_wr_issued_o,
    output logic [DS_ADDR_WIDTH-1:0] num_rd_issued_o
    );

   localparam CL_IDX_ODD = 1'b1;
   localparam CL_IDX_EVEN = 1'b0;
   // Gen Seq Al module signals.
   logic [DS_ADDR_WIDTH-1:0] 	     g_un_al_start_addr_i;
   logic [DS_ADDR_WIDTH-1:0] 	     g_num_cl_to_gen_i;
   logic 			     g_en_i;
   logic 			     g_done_o;
   logic [DS_ADDR_WIDTH-1:0] 	     g_al_addr_o;
   logic 			     g_valid_o;
   logic 			     g_ready_i;
   // ID register.
   logic [5-1:0] 		     g_rd_id_reg;
   // Odd, even filter signals.
   logic [5-1:0] 		     oe_rd_req_id_o;
   logic [DS_ADDR_WIDTH-1:0] 	     oe_rd_req_al_addr_o;
   logic 			     oe_rd_req_valid_o;
   logic 			     oe_rd_req_ready_i;
   // Modify read values signals.
   logic [5-1:0] 		     rmw_wr_id_o;
   logic [ECI_CL_WIDTH-1:0] 	     rmw_wr_data_o;
   logic 			     rmw_wr_valid_o;
   logic 			     rmw_wr_ready_i;
   // Performance counter
   enum {IDLE, RUN, STOP} state_reg, state_next;
   logic [DS_ADDR_WIDTH-1:0] num_rd_issued_reg = '0, num_rd_issued_next;
   logic [DS_ADDR_WIDTH-1:0] num_wr_issued_reg = '0, num_wr_issued_next;
   logic [DS_ADDR_WIDTH-1:0] tot_timer_reg = '0, tot_timer_next;
   logic [DS_ADDR_WIDTH-1:0] tot_num_rd_issued_reg = '0, tot_num_rd_issued_next;
   logic [DS_ADDR_WIDTH-1:0] tot_num_wr_issued_reg = '0, tot_num_wr_issued_next;
   
   always_comb begin : OP_ASSIGN
      rmw_done_o = (state_reg == STOP) ? 1'b1 : 1'b0;
      // Read request output. 
      f_rd_req_id_o = oe_rd_req_id_o;
      f_rd_req_al_addr_o = oe_rd_req_al_addr_o;
      f_rd_req_valid_o = oe_rd_req_valid_o;
      // read response. 
      f_rd_rsp_ready_o = rmw_wr_ready_i;
      // write request.
      f_wr_id_o = rmw_wr_id_o;
      f_wr_data_o = rmw_wr_data_o;
      f_wr_valid_o = rmw_wr_valid_o;
      // Timer.
      tot_timer_o = tot_timer_reg;
      num_wr_issued_o = tot_num_wr_issued_reg;
      num_rd_issued_o = tot_num_rd_issued_reg;
   end : OP_ASSIGN

   // Generate sequential aliased addresses and
   // filter them based on odd/even before issuing
   // read request.
   //
   // gen_seq_al:
   // Generate sequential un-aliased addresses and
   // alias it before sending downstream.
   always_comb begin : GSEQ_IP_ASSIGN
      g_un_al_start_addr_i = st_un_al_addr_i;
      g_num_cl_to_gen_i = num_cls_to_rmw_i;
      // g_en_i assigned in the state machine.
      g_ready_i = oe_rd_req_ready_i;
   end : GSEQ_IP_ASSIGN
   gen_seq_al
     seq_al_inst1
       (
	.clk(clk),
	.reset(reset),
	.un_al_start_addr_i(g_un_al_start_addr_i),
	.num_cl_to_gen_i(g_num_cl_to_gen_i),
	.en_i(g_en_i),
	.done_o(g_done_o),
	.al_addr_o(g_al_addr_o),
	.valid_o(g_valid_o),
	.ready_i(g_ready_i)
	);

   // Filter generated aliased address before sending downstream.
   // if ODD_GEN is 1 and generated aliased address is odd,
   // downstream is valid otherwise invalid.
   // if ODD_GEN is 0 and generated aliased address is even,
   // downstream is valid otherwise invalid. 
   always_comb begin : ODD_EVEN_FILTER
      oe_rd_req_id_o = g_rd_id_reg;
      oe_rd_req_al_addr_o = g_al_addr_o;
      oe_rd_req_valid_o = g_valid_o;
      if(ODD_GEN == 1'b1) begin
	 if(oe_rd_req_al_addr_o[ECI_CL_ADDR_LSB] == CL_IDX_ODD) begin
	    oe_rd_req_valid_o = g_valid_o;
	 end else begin
	    oe_rd_req_valid_o = 1'b0;
	 end
      end else begin
	 if(oe_rd_req_al_addr_o[ECI_CL_ADDR_LSB] == CL_IDX_ODD) begin
	    oe_rd_req_valid_o = 1'b0;
	 end else begin
	    oe_rd_req_valid_o = g_valid_o;
	 end
      end
      oe_rd_req_ready_i = f_rd_req_ready_i;
   end : ODD_EVEN_FILTER
   
   // whenever a read request is issued,
   // increment the ID for next read request.
   // If you want to serialize each read modify write
   // operation, dont increment ID. 
   always_ff @(posedge clk) begin : G_RD_ID_REG_ASSIGN
      if(state_reg == IDLE) begin
	 if(rmw_en_i) begin
	    g_rd_id_reg <= '0;
	 end
      end else begin
	 if(f_rd_req_valid_o & f_rd_req_ready_i) begin
	    //g_rd_id_reg <= g_rd_id_reg + 1;
	    // Serializing
	    g_rd_id_reg <= g_rd_id_reg;
	 end
      end
      if( reset ) begin
	 g_rd_id_reg <= '0;
      end
   end : G_RD_ID_REG_ASSIGN

   // Get read response and modify it before writing.
   always_comb begin : MODIFY_RD_VALUE
      rmw_wr_id_o = f_rd_rsp_id_i;
      rmw_wr_data_o = f_rd_rsp_data_i + 'd100;
      rmw_wr_valid_o = f_rd_rsp_valid_i;
      rmw_wr_ready_i = f_wr_ready_i;
   end : MODIFY_RD_VALUE

   // Latency Timer.
   // Measure latency from start till all writes are completed.
   //
   // We cannot tell the number of odd/even reads that will
   // be issued given the starting address and number of CLs.
   // So we keep track of number of reads issued and compare
   // it with number of writes to know when everything is done.
   // 
   // when module is enabled, reset the counters to 0
   // and start counting.
   // For every read request issued, increment read counter.
   // For every write request issued, increment write counter.
   // when gen_seq_al module is done with generating all
   // sequential addresses, we check if the number of
   // writes match number of reads to Stop the timer.
   always_comb begin : TIMER_CONTROLLER
      state_next = state_reg;
      num_rd_issued_next = num_rd_issued_reg;
      num_wr_issued_next = num_wr_issued_reg;
      tot_num_rd_issued_next = tot_num_rd_issued_reg;
      tot_num_wr_issued_next = tot_num_wr_issued_reg;
      tot_timer_next = tot_timer_reg;
      g_en_i = 1'b0;
      case(state_reg)
	IDLE: begin
	   // wait for module to be enabled
	   // before counting starts. 
	   if(rmw_en_i) begin
	      num_rd_issued_next = '0;
	      num_wr_issued_next = '0;
	      tot_num_rd_issued_next = '0;
	      tot_num_wr_issued_next = '0;
	      tot_timer_next = '0;
	      state_next = RUN;
	      g_en_i = 1'b1;
	   end
	end
	RUN: begin
	   // Count till all writes are done. 
	   if(f_rd_req_valid_o & f_rd_req_ready_i) begin
	      num_rd_issued_next = num_rd_issued_reg + 1;
	      tot_num_rd_issued_next = tot_num_rd_issued_reg + 1;
	   end
	   if(f_wr_valid_o & f_wr_ready_i) begin
	      num_wr_issued_next = num_wr_issued_reg + 1;
	      tot_num_wr_issued_next = tot_num_wr_issued_reg + 1;
	   end
	   tot_timer_next = tot_timer_reg + 1;
	   if(g_done_o) begin
	      // All read requests issued. 
	      if(num_wr_issued_reg == num_rd_issued_reg) begin
		 // all write requests issued.
		 // state_next = STOP;
		 // start a new iteration if still enabled.
		 if(rmw_en_i == 1'b1) begin
		    state_next = RUN;
		    num_rd_issued_next = '0;
		    num_wr_issued_next = '0;
		    // keep timer counting 
		    //tot_timer_next = '0;
		    g_en_i = 1'b1;
		    
		 end else begin
		    // no more iterations.
		    state_next = STOP;
		 end
	      end
	   end
	end
	STOP: begin
	   // be in this state till enable is disabled.
	   if(rmw_en_i == 1'b0) begin
	      state_next = IDLE;
	   end
	end
      endcase // case (state_reg)
   end : TIMER_CONTROLLER

   always_ff @(posedge clk) begin : REG_ASSIGN
      state_reg <= state_next;
      num_rd_issued_reg	<= num_rd_issued_next;
      num_wr_issued_reg <= num_wr_issued_next;
      tot_timer_reg	<= tot_timer_next;
      tot_num_rd_issued_reg <= tot_num_rd_issued_next;
      tot_num_wr_issued_reg <= tot_num_wr_issued_next;
      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset
	 state_reg <= IDLE;
	 num_rd_issued_reg <= '0;
	 num_wr_issued_reg <= '0;
	 tot_timer_reg <= '0;
	 tot_num_rd_issued_reg <= '0;
	 tot_num_wr_issued_reg <= '0;
      end
   end : REG_ASSIGN
   
endmodule // f_rmw_thread

`endif
