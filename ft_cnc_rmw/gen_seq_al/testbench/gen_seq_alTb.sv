import eci_cmd_defs::*;
import eci_dcs_defs::*;

module gen_seq_alTb();


   //input output ports 
   //Input signals
   logic 		      clk;
   logic 		      reset;
   logic [DS_ADDR_WIDTH-1:0]  un_al_start_addr_i;
   logic [DS_ADDR_WIDTH-1:0]  num_cl_to_gen_i;
   logic 		      en_i;
   logic 		      ready_i;

   //Output signals
   logic 		      done_o;
   logic [DS_ADDR_WIDTH-1:0]  al_addr_o;
   logic 		      valid_o;


   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   gen_seq_al gen_seq_al1 (
			   .clk(clk),
			   .reset(reset),
			   .un_al_start_addr_i(un_al_start_addr_i),
			   .num_cl_to_gen_i(num_cl_to_gen_i),
			   .en_i(en_i),
			   .ready_i(ready_i),
			   .done_o(done_o),
			   .al_addr_o(al_addr_o),
			   .valid_o(valid_o)
			   );//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports 
      clk = '0;
      reset = '1;
      un_al_start_addr_i = '0;
      num_cl_to_gen_i = '0;
      en_i = '0;
      ready_i = '0;
      ##5;
      reset = 1'b0;
      un_al_start_addr_i = '0;
      num_cl_to_gen_i = 'd5;
      en_i = '1;
      ready_i = '0;
      ##1;
      en_i = 1'b0;
      ##5;
      ready_i = '1;
      #500 $finish;
   end 

endmodule
