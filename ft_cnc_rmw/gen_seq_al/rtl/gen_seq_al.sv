/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-12-06
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */

`ifndef GEN_SEQ_AL_SV
 `define GEN_SEQ_AL_SV

import eci_cmd_defs::*;
import eci_dcs_defs::*;

// Provide a starting un-aliased address
// and number of addresses to generate as
// input and enable the module. the module
// generates the seq cl indices, aliases it
// and sends it via valid ready flow control.
//
// when all cl indices are sent, done is asserted.
// Note: sequential byte addresses are not
// generated rather the cl indices are sequential.

module gen_seq_al
  (
   input logic 			    clk,
   input logic 			    reset,
   // Incoming start address and
   // number of cls to generate.
   input logic [DS_ADDR_WIDTH-1:0]  un_al_start_addr_i,
   input logic [DS_ADDR_WIDTH-1:0]  num_cl_to_gen_i,
   input logic 			    en_i,
   output logic 		    done_o,
   // Output aliased seq cl indices
   output logic [DS_ADDR_WIDTH-1:0] al_addr_o,
   output logic 		    valid_o,
   input logic 			    ready_i
   );

   enum 			    {DONE, WIP} state_reg, state_next;
   logic 			    done_sig;
   logic 			    valid_sig;
   logic [DS_ADDR_WIDTH-1:0] 	    un_al_addr_reg = '0, un_al_addr_next;
   logic [DS_ADDR_WIDTH-1:0] 	    num_cl_to_gen_reg = '0, num_cl_to_gen_next;
   logic [DS_ADDR_WIDTH-1:0] 	    out_counter_reg = '0, out_counter_next;
   
   always_comb begin : OP_ASSIGN
      done_o = done_sig;
      al_addr_o = f_alias_dcs_addr(
				   .un_al_dcs_addr_i(un_al_addr_reg)
				   );
      valid_o = valid_sig;
   end : OP_ASSIGN

   always_comb begin : CONTROLLER
      done_sig = 1'b0;
      valid_sig = 1'b0;
      state_next = state_reg;
      un_al_addr_next = un_al_addr_reg;
      num_cl_to_gen_next = num_cl_to_gen_reg;
      out_counter_next = out_counter_reg;
      case(state_reg)
	DONE: begin
	   // ready to accept new input. 
	   done_sig = 1'b1;
	   if(en_i) begin
	      state_next = WIP;
	      un_al_addr_next = un_al_start_addr_i;
	      num_cl_to_gen_next =  num_cl_to_gen_i;
	      out_counter_next = '0;
	   end
	end
	WIP: begin
	   // start sending out addresses.
	   valid_sig = 1'b1;
	   if(valid_o & ready_i) begin
	      un_al_addr_next = un_al_addr_reg + ECI_CL_SIZE_BYTES;
	      out_counter_next = out_counter_reg + 'd1;
	      if(out_counter_next == num_cl_to_gen_reg) begin
		 state_next = DONE;
	      end
	   end
	end
      endcase
   end : CONTROLLER

   // state reg should be assigned.
   always_ff @(posedge clk) begin : REG_ASSIGN
      state_reg <= state_next;
      un_al_addr_reg	<= un_al_addr_next;
      num_cl_to_gen_reg <= num_cl_to_gen_next;
      out_counter_reg	<= out_counter_next;
      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset 
	 //un_al_addr_reg	<= '0;
	 //num_cl_to_gen_reg	<= '0;
	 state_reg <= DONE;
	 out_counter_reg <= '0;
      end
   end : REG_ASSIGN

   // Alias an unaliased DCS address.
   function automatic [DS_ADDR_WIDTH-1:0] f_alias_dcs_addr
     (
      input logic [DS_ADDR_WIDTH-1:0] un_al_dcs_addr_i
      );
      eci_cl_addr_t un_al_addr_c, al_addr_c;
      logic [DS_ADDR_WIDTH-1:0]       al_dcs_addr;
      un_al_addr_c.flat = '0;
      un_al_addr_c.flat[DS_ADDR_WIDTH-1:0] = un_al_dcs_addr_i;
      al_addr_c.flat = '0;
      al_addr_c.parts.cl_index = eci_cmd_defs::eci_alias_cache_line_index
				 (
				  .cli(un_al_addr_c.parts.cl_index)
				  );
      al_dcs_addr = al_addr_c.flat[DS_ADDR_WIDTH-1:0];
      return(al_dcs_addr);
   endfunction : f_alias_dcs_addr
   
endmodule // gen_seq_al

`endif
