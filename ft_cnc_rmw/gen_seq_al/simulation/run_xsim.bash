#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
      $XILINX_VIVADO/data/verilog/src/glbl.v \
      ../rtl/eci_cmd_defs.sv \
      ../rtl/eci_dcs_defs.sv \
      ../testbench/gen_seq_alTb.sv \
      ../rtl/gen_seq_al.sv \

xelab -debug typical -incremental -L xpm worklib.gen_seq_alTb worklib.glbl -s worklib.gen_seq_alTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.gen_seq_alTb 
xsim -gui worklib.gen_seq_alTb 
