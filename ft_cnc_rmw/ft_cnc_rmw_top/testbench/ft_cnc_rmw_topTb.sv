import eci_cmd_defs::*; 
import eci_dcs_defs::*; 

module ft_cnc_rmw_topTb();

   parameter logic ODD_GEN = 1'b1;
   localparam CL_IDX_ODD = 1'b1;
   localparam CL_IDX_EVEN = 1'b0;


   //input output ports 
   //Input signals
   logic 	   clk;
   logic 	   reset;
   logic [DS_ADDR_WIDTH-1:0] st_un_al_addr_i;
   logic [DS_ADDR_WIDTH-1:0] num_cls_to_rmw_i;
   logic 		     rmw_en_i;
   logic [MAX_DCU_ID_WIDTH-1:0] dc_rd_req_id_i;
   logic [DS_ADDR_WIDTH-1:0] 	dc_rd_req_addr_i;
   logic 			dc_rd_req_valid_i;
   logic 			dc_rd_rsp_ready_i;
   logic [MAX_DCU_ID_WIDTH-1:0] dc_wr_req_id_i;
   logic [DS_ADDR_WIDTH-1:0] 	dc_wr_req_addr_i;
   logic [ECI_CL_WIDTH-1:0] 	dc_wr_req_data_i;
   logic [ECI_CL_SIZE_BYTES-1:0] dc_wr_req_strb_i;
   logic 			 dc_wr_req_valid_i;
   logic 			 dc_wr_rsp_ready_i;
   logic 			 rd_req_ready_i;
   logic [MAX_DCU_ID_WIDTH-1:0]  rd_rsp_id_i;
   logic [ECI_CL_WIDTH-1:0] 	 rd_rsp_data_i;
   logic 			 rd_rsp_valid_i;
   logic 			 wr_req_ready_i;
   logic [MAX_DCU_ID_WIDTH-1:0]  wr_rsp_id_i;
   logic [1:0] 			 wr_rsp_bresp_i;
   logic 			 wr_rsp_valid_i;
   logic 			 lcl_fwd_wod_pkt_ready_i;
   logic [ECI_WORD_WIDTH-1:0] 	 lcl_rsp_wod_hdr_i;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] lcl_rsp_wod_pkt_size_i;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] lcl_rsp_wod_pkt_vc_i;
   logic 				 lcl_rsp_wod_pkt_valid_i;
   logic 				 lcl_rsp_wod_pkt_ready_i;

   //Output signals
   logic 				 rmw_done_o;
   logic [DS_ADDR_WIDTH-1:0] 		 tot_timer_o;
   logic [DS_ADDR_WIDTH-1:0] 		 num_wr_issued_o;
   logic [DS_ADDR_WIDTH-1:0] 		 num_rd_issued_o;
   logic 				 dc_rd_req_ready_o;
   logic [MAX_DCU_ID_WIDTH-1:0] 	 dc_rd_rsp_id_o;
   logic [ECI_CL_WIDTH-1:0] 		 dc_rd_rsp_data_o;
   logic 				 dc_rd_rsp_valid_o;
   logic 				 dc_wr_req_ready_o;
   logic [MAX_DCU_ID_WIDTH-1:0] 	 dc_wr_rsp_id_o;
   logic [1:0] 				 dc_wr_rsp_bresp_o;
   logic 				 dc_wr_rsp_valid_o;
   logic [MAX_DCU_ID_WIDTH-1:0] 	 rd_req_id_o;
   logic [DS_ADDR_WIDTH-1:0] 		 rd_req_addr_o;
   logic 				 rd_req_valid_o;
   logic 				 rd_rsp_ready_o;
   logic [MAX_DCU_ID_WIDTH-1:0] 	 wr_req_id_o;
   logic [DS_ADDR_WIDTH-1:0] 		 wr_req_addr_o;
   logic [ECI_CL_WIDTH-1:0] 		 wr_req_data_o;
   logic [ECI_CL_SIZE_BYTES-1:0] 	 wr_req_strb_o;
   logic 				 wr_req_valid_o;
   logic 				 wr_rsp_ready_o;
   logic [ECI_WORD_WIDTH-1:0] 		 lcl_fwd_wod_hdr_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 lcl_fwd_wod_pkt_size_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] lcl_fwd_wod_pkt_vc_o;
   logic 				 lcl_fwd_wod_pkt_valid_o;
   logic 				 lcl_rsp_wod_pkt_ready_o;
   logic [ECI_WORD_WIDTH-1:0] 		 lcl_rsp_wod_hdr_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 	 lcl_rsp_wod_pkt_size_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] lcl_rsp_wod_pkt_vc_o;
   logic 				 lcl_rsp_wod_pkt_valid_o;


   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   ft_cnc_rmw_top ft_cnc_rmw_top1 (
				   .clk(clk),
				   .reset(reset),
				   .st_un_al_addr_i(st_un_al_addr_i),
				   .num_cls_to_rmw_i(num_cls_to_rmw_i),
				   .rmw_en_i(rmw_en_i),
				   .dc_rd_req_id_i(dc_rd_req_id_i),
				   .dc_rd_req_addr_i(dc_rd_req_addr_i),
				   .dc_rd_req_valid_i(dc_rd_req_valid_i),
				   .dc_rd_rsp_ready_i(dc_rd_rsp_ready_i),
				   .dc_wr_req_id_i(dc_wr_req_id_i),
				   .dc_wr_req_addr_i(dc_wr_req_addr_i),
				   .dc_wr_req_data_i(dc_wr_req_data_i),
				   .dc_wr_req_strb_i(dc_wr_req_strb_i),
				   .dc_wr_req_valid_i(dc_wr_req_valid_i),
				   .dc_wr_rsp_ready_i(dc_wr_rsp_ready_i),
				   .rd_req_ready_i(rd_req_ready_i),
				   .rd_rsp_id_i(rd_rsp_id_i),
				   .rd_rsp_data_i(rd_rsp_data_i),
				   .rd_rsp_valid_i(rd_rsp_valid_i),
				   .wr_req_ready_i(wr_req_ready_i),
				   .wr_rsp_id_i(wr_rsp_id_i),
				   .wr_rsp_bresp_i(wr_rsp_bresp_i),
				   .wr_rsp_valid_i(wr_rsp_valid_i),
				   .lcl_fwd_wod_pkt_ready_i(lcl_fwd_wod_pkt_ready_i),
				   .lcl_rsp_wod_hdr_i(lcl_rsp_wod_hdr_i),
				   .lcl_rsp_wod_pkt_size_i(lcl_rsp_wod_pkt_size_i),
				   .lcl_rsp_wod_pkt_vc_i(lcl_rsp_wod_pkt_vc_i),
				   .lcl_rsp_wod_pkt_valid_i(lcl_rsp_wod_pkt_valid_i),
				   .lcl_rsp_wod_pkt_ready_i(lcl_rsp_wod_pkt_ready_i),
				   .rmw_done_o(rmw_done_o),
				   .tot_timer_o(tot_timer_o),
				   .num_wr_issued_o(num_wr_issued_o),
				   .num_rd_issued_o(num_rd_issued_o),
				   .dc_rd_req_ready_o(dc_rd_req_ready_o),
				   .dc_rd_rsp_id_o(dc_rd_rsp_id_o),
				   .dc_rd_rsp_data_o(dc_rd_rsp_data_o),
				   .dc_rd_rsp_valid_o(dc_rd_rsp_valid_o),
				   .dc_wr_req_ready_o(dc_wr_req_ready_o),
				   .dc_wr_rsp_id_o(dc_wr_rsp_id_o),
				   .dc_wr_rsp_bresp_o(dc_wr_rsp_bresp_o),
				   .dc_wr_rsp_valid_o(dc_wr_rsp_valid_o),
				   .rd_req_id_o(rd_req_id_o),
				   .rd_req_addr_o(rd_req_addr_o),
				   .rd_req_valid_o(rd_req_valid_o),
				   .rd_rsp_ready_o(rd_rsp_ready_o),
				   .wr_req_id_o(wr_req_id_o),
				   .wr_req_addr_o(wr_req_addr_o),
				   .wr_req_data_o(wr_req_data_o),
				   .wr_req_strb_o(wr_req_strb_o),
				   .wr_req_valid_o(wr_req_valid_o),
				   .wr_rsp_ready_o(wr_rsp_ready_o),
				   .lcl_fwd_wod_hdr_o(lcl_fwd_wod_hdr_o),
				   .lcl_fwd_wod_pkt_size_o(lcl_fwd_wod_pkt_size_o),
				   .lcl_fwd_wod_pkt_vc_o(lcl_fwd_wod_pkt_vc_o),
				   .lcl_fwd_wod_pkt_valid_o(lcl_fwd_wod_pkt_valid_o),
				   .lcl_rsp_wod_pkt_ready_o(lcl_rsp_wod_pkt_ready_o),
				   .lcl_rsp_wod_hdr_o(lcl_rsp_wod_hdr_o),
				   .lcl_rsp_wod_pkt_size_o(lcl_rsp_wod_pkt_size_o),
				   .lcl_rsp_wod_pkt_vc_o(lcl_rsp_wod_pkt_vc_o),
				   .lcl_rsp_wod_pkt_valid_o(lcl_rsp_wod_pkt_valid_o)
				   );//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports 
      clk = '0;
      reset = '1;
      st_un_al_addr_i = '0;
      num_cls_to_rmw_i = '0;
      rmw_en_i = '0;
      dc_rd_req_id_i = '0;
      dc_rd_req_addr_i = '0;
      dc_rd_req_valid_i = '0;
      dc_rd_rsp_ready_i = '0;
      dc_wr_req_id_i = '0;
      dc_wr_req_addr_i = '0;
      dc_wr_req_data_i = '0;
      dc_wr_req_strb_i = '0;
      dc_wr_req_valid_i = '0;
      dc_wr_rsp_ready_i = '0;
      lcl_rsp_wod_pkt_ready_i = '0;

      ##5;
      reset = 1'b0;
      lcl_rsp_wod_pkt_ready_i = '1;
      ##5;

      // Issue a request and it keeps running
      // forever till disabled. When disabled,
      // renable only after done signal is asserted.
      st_un_al_addr_i = 'd0;
      num_cls_to_rmw_i = 'd4;
      rmw_en_i = 1'b1;
      ##100;
      rmw_en_i = 1'b0;
      wait(rmw_done_o);
      ##1;
      ##5;

      st_un_al_addr_i = 'd0;
      num_cls_to_rmw_i = 'd4;
      rmw_en_i = 1'b1;
      ##50;
      rmw_en_i = 1'b0;
      wait(rmw_done_o);
      ##1;

      
      #500 $finish;
   end

   // LCI LCIA loopback.
   always_comb begin : LCI_LCIA_LOOPBACK
      lcl_rsp_wod_hdr_i = eci_cmd_defs::lcl_gen_lcia(lcl_fwd_wod_hdr_o);
      lcl_rsp_wod_pkt_size_i = 'd1;
      if(lcl_rsp_wod_hdr_i[ECI_CL_ADDR_LSB] == CL_IDX_ODD) begin
	 lcl_rsp_wod_pkt_vc_i = VC_LCL_RESP_WO_DATA_E; //18
      end else begin
	 lcl_rsp_wod_pkt_vc_i = VC_LCL_RESP_WO_DATA_O; //19
      end
      lcl_rsp_wod_pkt_valid_i = lcl_fwd_wod_pkt_valid_o;
      lcl_fwd_wod_pkt_ready_i = lcl_rsp_wod_pkt_ready_o;
   end : LCI_LCIA_LOOPBACK

   // This is word addressable memory.
   // An address points to a CL.
   // Give CL index as input to this module.
   // It can hold 2^10 CLs. (only for sim).
   word_addr_mem #
     (
      .ID_WIDTH(MAX_DCU_ID_WIDTH),
      .ADDR_WIDTH(10),
      .DATA_WIDTH(ECI_CL_WIDTH)
       )
   mem_inst_1
     (
      .clk(clk),
      .reset(reset),

      .rd_req_id_i(rd_req_id_o),
      .rd_req_addr_i(rd_req_addr_o[9+7:7]),
      .rd_req_valid_i(rd_req_valid_o),
      .rd_req_ready_o(rd_req_ready_i),

      .wr_req_id_i(wr_req_id_o),
      .wr_req_addr_i(wr_req_addr_o[9+7:7]),
      .wr_req_data_i(wr_req_data_o),
      .wr_req_valid_i(wr_req_valid_o),
      .wr_req_ready_o(wr_req_ready_i),

      .rd_rsp_id_o(rd_rsp_id_i),
      .rd_rsp_data_o(rd_rsp_data_i),
      .rd_rsp_valid_o(rd_rsp_valid_i),
      .rd_rsp_ready_i(rd_rsp_ready_o),

      .wr_rsp_id_o(wr_rsp_id_i),
      .wr_rsp_bresp_o(wr_rsp_bresp_i),
      .wr_rsp_valid_o(wr_rsp_valid_i),
      .wr_rsp_ready_i(wr_rsp_ready_o)
      );


endmodule
