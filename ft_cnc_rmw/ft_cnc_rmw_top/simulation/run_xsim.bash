#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
      $XILINX_VIVADO/data/verilog/src/glbl.v \
      ../rtl/eci_cmd_defs.sv \
      ../rtl/eci_dcs_defs.sv \
      ../testbench/word_addr_mem.sv \
      ../testbench/ft_cnc_rmw_topTb.sv \
      ../rtl/ft_cnc_rmw_top.sv \
      ../rtl/ft_dc_desc_arb.sv \
      ../rtl/f_rmw_thread.sv \
      ../rtl/axis_2_router.sv \
      ../rtl/gen_seq_al.sv \
      ../rtl/axis_comb_rr_arb.sv \
      ../rtl/ft_cnc_rmw.sv \
      ../rtl/axis_pipeline_stage.sv 

xelab -debug typical -incremental -L xpm worklib.ft_cnc_rmw_topTb worklib.glbl -s worklib.ft_cnc_rmw_topTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.ft_cnc_rmw_topTb 
xsim -gui worklib.ft_cnc_rmw_topTb 
