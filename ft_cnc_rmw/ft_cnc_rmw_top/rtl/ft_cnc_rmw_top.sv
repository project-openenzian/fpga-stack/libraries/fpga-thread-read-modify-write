/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2023-02-28
 * Project : Enzian
 *
 * Copyright (c) 2023, ETH Zurich.  All rights reserved.
 *
 */

`ifndef FT_CNC_RMW_TOP_SV
`define FT_CNC_RMW_TOP_SV

import eci_cmd_defs::*; // ECI definitions.
import eci_dcs_defs::*; // DCS definitions.

// Given start address and num CLs, instantiate
// an FPGA thread to perform read modify write.
//
// The requests from the FPGA threads are converted
// to clean invalidate + lock, and when clean inv ack
// arrives, read request is sent.
// when read response arrives, data is modified,
// written back. when wr rsp from memory arrives,
// the cacheline is unlocked.
//
// Module also arbitrates between desc from DC
// and FPGA threads to choose 1 to issue to memory.

module ft_cnc_rmw_top #
  (
   parameter logic ODD_GEN = 1'b1
   )
  (
   input logic 					clk,
   input logic 					reset,
   // Input read request from FPGA threads.
   input logic [DS_ADDR_WIDTH-1:0] 		st_un_al_addr_i,
   input logic [DS_ADDR_WIDTH-1:0] 		num_cls_to_rmw_i,
   input logic 					rmw_en_i,
   output logic 				rmw_done_o,

   // Output performance counters.
   output logic [DS_ADDR_WIDTH-1:0] 		tot_timer_o,
   output logic [DS_ADDR_WIDTH-1:0] 		num_wr_issued_o,
   output logic [DS_ADDR_WIDTH-1:0] 		num_rd_issued_o,

   // DC desc inputs.
   // Output Read descriptors
   // Read descriptors: Request and response.
   input logic [MAX_DCU_ID_WIDTH-1:0] 		dc_rd_req_id_i,
   input logic [DS_ADDR_WIDTH-1:0] 		dc_rd_req_addr_i,
   input logic 					dc_rd_req_valid_i,
   output logic 				dc_rd_req_ready_o,

   output logic [MAX_DCU_ID_WIDTH-1:0] 		dc_rd_rsp_id_o,
   output logic [ECI_CL_WIDTH-1:0] 		dc_rd_rsp_data_o,
   output logic 				dc_rd_rsp_valid_o,
   input logic 					dc_rd_rsp_ready_i,

    // Write descriptors: Request and response.
   input logic [MAX_DCU_ID_WIDTH-1:0] 		dc_wr_req_id_i,
   input logic [DS_ADDR_WIDTH-1:0] 		dc_wr_req_addr_i,
   input logic [ECI_CL_WIDTH-1:0] 		dc_wr_req_data_i,
   input logic [ECI_CL_SIZE_BYTES-1:0] 		dc_wr_req_strb_i,
   input logic 					dc_wr_req_valid_i,
   output logic 				dc_wr_req_ready_o,

   output logic [MAX_DCU_ID_WIDTH-1:0] 		dc_wr_rsp_id_o,
   output logic [1:0] 				dc_wr_rsp_bresp_o,
   output logic 				dc_wr_rsp_valid_o,
   input logic 					dc_wr_rsp_ready_i,

   // Output arbitrated read descriptor to mem.
   output logic [MAX_DCU_ID_WIDTH-1:0] 		rd_req_id_o,
   output logic [DS_ADDR_WIDTH-1:0] 		rd_req_addr_o,
   output logic 				rd_req_valid_o,
   input logic 					rd_req_ready_i,

   // Input read response from mem.
   input logic [MAX_DCU_ID_WIDTH-1:0] 		rd_rsp_id_i,
   input logic [ECI_CL_WIDTH-1:0] 		rd_rsp_data_i,
   input logic 					rd_rsp_valid_i,
   output logic 				rd_rsp_ready_o,

   // Output write request to memory.
   output logic [MAX_DCU_ID_WIDTH-1:0] 		wr_req_id_o,
   output logic [DS_ADDR_WIDTH-1:0] 		wr_req_addr_o,
   output logic [ECI_CL_WIDTH-1:0] 		wr_req_data_o,
   output logic [ECI_CL_SIZE_BYTES-1:0] 	wr_req_strb_o,
   output logic 				wr_req_valid_o,
   input logic 					wr_req_ready_i,

   // Input write response from memory.
   input logic [MAX_DCU_ID_WIDTH-1:0] 		wr_rsp_id_i,
   input logic [1:0] 				wr_rsp_bresp_i,
   input logic 					wr_rsp_valid_i,
   output logic 				wr_rsp_ready_o,

   // Output LCI VC 16 or 17
   output logic [ECI_WORD_WIDTH-1:0] 		lcl_fwd_wod_hdr_o,
   output logic [ECI_PACKET_SIZE_WIDTH-1:0] 	lcl_fwd_wod_pkt_size_o,
   output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] lcl_fwd_wod_pkt_vc_o,
   output logic 				lcl_fwd_wod_pkt_valid_o,
   input logic 					lcl_fwd_wod_pkt_ready_i,
   // Input LCIA VC 18 or 19.
   input logic [ECI_WORD_WIDTH-1:0] 		lcl_rsp_wod_hdr_i,
   input logic [ECI_PACKET_SIZE_WIDTH-1:0] 	lcl_rsp_wod_pkt_size_i,
   input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	lcl_rsp_wod_pkt_vc_i,
   input logic 					lcl_rsp_wod_pkt_valid_i,
   output logic 				lcl_rsp_wod_pkt_ready_o,
   // Output unlock VC 18 or 19.
   output logic [ECI_WORD_WIDTH-1:0] 		lcl_rsp_wod_hdr_o,
   output logic [ECI_PACKET_SIZE_WIDTH-1:0] 	lcl_rsp_wod_pkt_size_o,
   output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] lcl_rsp_wod_pkt_vc_o,
   output logic 				lcl_rsp_wod_pkt_valid_o,
   input logic 					lcl_rsp_wod_pkt_ready_i
   );

   typedef struct packed {
      logic [ECI_WORD_WIDTH-1:0]            hdr;
      logic [ECI_PACKET_SIZE_WIDTH-1:0]     size;
      logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] vc;
      logic				    valid;
      logic				    ready;
   } eci_hdr_if_t;

   typedef struct packed {
      logic [ECI_PACKET_SIZE-1:0][ECI_WORD_WIDTH-1:0] pkt;
      logic [ECI_PACKET_SIZE_WIDTH-1:0]		      size;
      logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0]	      vc;
      logic					      valid;
      logic					      ready;
   } eci_pkt_if_t;

   typedef struct packed {
      logic [MAX_DCU_ID_WIDTH-1:0] id;
      logic [DS_ADDR_WIDTH-1:0]    addr;
      logic			   valid;
      logic			   ready;
   } rd_req_ctrl_if_t;

   typedef struct packed {
      logic [MAX_DCU_ID_WIDTH-1:0] id;
      logic			   valid;
      logic			   ready;
   } rd_rsp_ctrl_if_t;

   typedef logic [ECI_CL_WIDTH-1:0] rd_rsp_data_if_t;

   typedef struct packed {
      logic [MAX_DCU_ID_WIDTH-1:0]  id;
      logic [DS_ADDR_WIDTH-1:0]     addr;
      logic [ECI_CL_SIZE_BYTES-1:0] strb;
      logic			    valid;
      logic			    ready;
   } wr_req_ctrl_if_t;

   typedef logic [ECI_CL_WIDTH-1:0] wr_req_data_if_t;

   typedef struct packed {
      logic [MAX_DCU_ID_WIDTH-1:0]  id;
      logic [1:0]		    bresp;
      logic			    valid;
      logic			    ready;
   } wr_rsp_ctrl_if_t;


   // FPGA thread req, rsp interfaces.
   // ID is 5 bits instead of 7.
   typedef struct		    packed {
      logic [5-1:0] id;
      logic [DS_ADDR_WIDTH-1:0]    addr;
      logic			   valid;
      logic			   ready;
   } ft_rd_req_ctrl_if_t;

   typedef struct packed {
      logic [5-1:0] id;
      logic			   valid;
      logic			   ready;
   } ft_rd_rsp_ctrl_if_t;

   typedef logic [ECI_CL_WIDTH-1:0] ft_rd_rsp_data_if_t;

   typedef struct packed {
      logic [5-1:0]  id;
      logic [DS_ADDR_WIDTH-1:0]     addr;
      logic [ECI_CL_SIZE_BYTES-1:0] strb;
      logic			    valid;
      logic			    ready;
   } ft_wr_req_ctrl_if_t;

   typedef logic [ECI_CL_WIDTH-1:0] ft_wr_req_data_if_t;

   typedef struct packed {
      logic [5-1:0]  id;
      logic [1:0]		    bresp;
      logic			    valid;
      logic			    ready;
   } ft_wr_rsp_ctrl_if_t;

   // FPGA thread signals.
   ft_rd_req_ctrl_if_t gen_rd_req_o;
   ft_rd_rsp_ctrl_if_t gen_rd_rsp_ctrl_i;
   ft_rd_rsp_data_if_t gen_rd_rsp_data_i;
   ft_wr_req_ctrl_if_t gen_wr_req_ctrl_o;
   ft_wr_req_data_if_t gen_wr_req_data_o;
   // gen CNC rmw signals.
   ft_rd_req_ctrl_if_t gen_rd_req_i;
   ft_rd_rsp_ctrl_if_t gen_rd_rsp_ctrl_o;
   ft_rd_rsp_data_if_t gen_rd_rsp_data_o;
   ft_wr_req_ctrl_if_t gen_wr_req_ctrl_i;
   ft_wr_req_data_if_t gen_wr_req_data_i;
   // FT cnc rmw signals.
   ft_rd_req_ctrl_if_t ft_rd_req_o;
   ft_rd_rsp_ctrl_if_t ft_rd_rsp_ctrl_i;
   ft_rd_rsp_data_if_t ft_rd_rsp_data_i;
   ft_wr_req_ctrl_if_t ft_wr_req_ctrl_o;
   ft_wr_req_data_if_t ft_wr_req_data_o;
   ft_wr_rsp_ctrl_if_t ft_wr_rsp_ctrl_i;
   // FT DC signals.
   eci_hdr_if_t ft_lcl_fwd_wod_o;
   eci_hdr_if_t ft_lcl_rsp_wod_i;
   eci_hdr_if_t ft_lcl_rsp_wod_o;
   // Arb FT cnc rmw signals.
   ft_rd_req_ctrl_if_t arb_ft_rd_req_i;
   ft_rd_rsp_ctrl_if_t arb_ft_rd_rsp_ctrl_o;
   ft_rd_rsp_data_if_t arb_ft_rd_rsp_data_o;
   ft_wr_req_ctrl_if_t arb_ft_wr_req_ctrl_i;
   ft_wr_req_data_if_t arb_ft_wr_req_data_i;
   ft_wr_rsp_ctrl_if_t arb_ft_wr_rsp_ctrl_o;
   // Arb output.
   rd_req_ctrl_if_t arb_rd_req_o;
   rd_rsp_ctrl_if_t arb_rd_rsp_ctrl_i;
   rd_rsp_data_if_t arb_rd_rsp_data_i;
   wr_req_ctrl_if_t arb_wr_req_ctrl_o;
   wr_req_data_if_t arb_wr_req_data_o;
   wr_rsp_ctrl_if_t arb_wr_rsp_ctrl_i;

   // Output assign.
   always_comb begin : OP_ASSIGN
      // Output LCI VC 16 or 17
      lcl_fwd_wod_hdr_o		= ft_lcl_fwd_wod_o.hdr;
      lcl_fwd_wod_pkt_size_o	= ft_lcl_fwd_wod_o.size;
      lcl_fwd_wod_pkt_vc_o	= ft_lcl_fwd_wod_o.vc;
      lcl_fwd_wod_pkt_valid_o	= ft_lcl_fwd_wod_o.valid;
      // Input LCIA VC 18 or 19.
      lcl_rsp_wod_pkt_ready_o	= ft_lcl_rsp_wod_i.ready;
      // Output unlock VC 18 or 19.
      lcl_rsp_wod_hdr_o		= ft_lcl_rsp_wod_o.hdr;
      lcl_rsp_wod_pkt_size_o	= ft_lcl_rsp_wod_o.size;
      lcl_rsp_wod_pkt_vc_o	= ft_lcl_rsp_wod_o.vc;
      lcl_rsp_wod_pkt_valid_o	= ft_lcl_rsp_wod_o.valid;
      // Output rd, wr desc to mem.
      rd_req_id_o		= arb_rd_req_o.id;
      rd_req_addr_o		= arb_rd_req_o.addr;
      rd_req_valid_o		= arb_rd_req_o.valid;
      rd_rsp_ready_o		= arb_rd_rsp_ctrl_i.ready;
      wr_req_id_o		= arb_wr_req_ctrl_o.id;
      wr_req_addr_o		= arb_wr_req_ctrl_o.addr;
      wr_req_data_o		= arb_wr_req_data_o;
      wr_req_strb_o		= arb_wr_req_ctrl_o.strb;
      wr_req_valid_o		= arb_wr_req_ctrl_o.valid;
      wr_rsp_ready_o		= arb_wr_rsp_ctrl_i.ready;
   end : OP_ASSIGN

   // Generate read modify write request
   // by FPGA thread.
   always_comb begin : FPGA_THREAD_IP_ASSIGN
      // rd req op.
      gen_rd_req_o.ready	= gen_rd_req_i.ready;
      // rd rsp ip.
      gen_rd_rsp_ctrl_i.id	= gen_rd_rsp_ctrl_o.id;
      gen_rd_rsp_data_i		= gen_rd_rsp_data_o;
      gen_rd_rsp_ctrl_i.valid	= gen_rd_rsp_ctrl_o.valid;
      // wr req.
      gen_wr_req_ctrl_o.ready	= gen_wr_req_ctrl_i.ready;
   end : FPGA_THREAD_IP_ASSIGN
   f_rmw_thread #
     (
      .ODD_GEN(ODD_GEN)
       )
   fpga_thread1
     (
      .clk			(clk),
      .reset			(reset),
      .st_un_al_addr_i		(st_un_al_addr_i),
      .num_cls_to_rmw_i		(num_cls_to_rmw_i),
      .rmw_en_i			(rmw_en_i),
      .rmw_done_o		(rmw_done_o),
      .f_rd_req_id_o		(gen_rd_req_o.id),
      .f_rd_req_al_addr_o	(gen_rd_req_o.addr),
      .f_rd_req_valid_o		(gen_rd_req_o.valid),
      .f_rd_req_ready_i		(gen_rd_req_o.ready),
      .f_rd_rsp_id_i		(gen_rd_rsp_ctrl_i.id),
      .f_rd_rsp_data_i		(gen_rd_rsp_data_i),
      .f_rd_rsp_valid_i		(gen_rd_rsp_ctrl_i.valid),
      .f_rd_rsp_ready_o		(gen_rd_rsp_ctrl_i.ready),
      .f_wr_id_o		(gen_wr_req_ctrl_o.id),
      .f_wr_data_o		(gen_wr_req_data_o),
      .f_wr_valid_o		(gen_wr_req_ctrl_o.valid),
      .f_wr_ready_i		(gen_wr_req_ctrl_o.ready),
      .tot_timer_o		(tot_timer_o),
      .num_wr_issued_o		(num_wr_issued_o),
      .num_rd_issued_o		(num_rd_issued_o)
      );

   // Perform coherent non caching read write,
   // from desc generated by FPGA threads.
   always_comb begin : FT_CNC_IP_ASSIGN
      // gen rd req.
      gen_rd_req_i.id		= gen_rd_req_o.id;
      gen_rd_req_i.addr		= gen_rd_req_o.addr;
      gen_rd_req_i.valid	= gen_rd_req_o.valid;
      // gen rd rsp.
      gen_rd_rsp_ctrl_o.ready	= gen_rd_rsp_ctrl_i.ready;
      // gen wr req.
      gen_wr_req_ctrl_i.id	= gen_wr_req_ctrl_o.id;
      gen_wr_req_data_i		= gen_wr_req_data_o;
      gen_wr_req_ctrl_i.valid	= gen_wr_req_ctrl_o.valid;
      // FT rd req output.
      ft_rd_req_o.ready		= arb_ft_rd_req_i.ready;
      // FT rd rsp input.
      ft_rd_rsp_ctrl_i.id	= arb_ft_rd_rsp_ctrl_o.id;
      ft_rd_rsp_data_i		= arb_ft_rd_rsp_data_o;
      ft_rd_rsp_ctrl_i.valid	= arb_ft_rd_rsp_ctrl_o.valid;
      // FT wr req output.
      ft_wr_req_ctrl_o.ready	= arb_ft_wr_req_ctrl_i.ready;
      // FT wr rsp input.
      ft_wr_rsp_ctrl_i.id	= arb_ft_wr_rsp_ctrl_o.id;
      ft_wr_rsp_ctrl_i.bresp	= arb_ft_wr_rsp_ctrl_o.bresp;
      ft_wr_rsp_ctrl_i.valid	= arb_ft_wr_rsp_ctrl_o.valid;
      // FT LCI.
      ft_lcl_fwd_wod_o.ready	= lcl_fwd_wod_pkt_ready_i;
      // FT LCIA.
      ft_lcl_rsp_wod_i.hdr	= lcl_rsp_wod_hdr_i;
      ft_lcl_rsp_wod_i.size	= lcl_rsp_wod_pkt_size_i;
      ft_lcl_rsp_wod_i.vc	= lcl_rsp_wod_pkt_vc_i;
      ft_lcl_rsp_wod_i.valid	= lcl_rsp_wod_pkt_valid_i;
      // unlock.
      ft_lcl_rsp_wod_o.ready	= lcl_rsp_wod_pkt_ready_i;
   end : FT_CNC_IP_ASSIGN
   ft_cnc_rmw
     ft_cnc_rmw_inst
     (
      .clk			(clk),
      .reset			(reset),
      .f_rd_req_id_i		(gen_rd_req_i.id),
      .f_rd_req_al_addr_i	(gen_rd_req_i.addr),
      .f_rd_req_valid_i		(gen_rd_req_i.valid),
      .f_rd_req_ready_o		(gen_rd_req_i.ready),
      .f_rd_rsp_id_o		(gen_rd_rsp_ctrl_o.id),
      .f_rd_rsp_data_o		(gen_rd_rsp_data_o),
      .f_rd_rsp_valid_o		(gen_rd_rsp_ctrl_o.valid),
      .f_rd_rsp_ready_i		(gen_rd_rsp_ctrl_o.ready),
      .f_wr_id_i		(gen_wr_req_ctrl_i.id),
      .f_wr_data_i		(gen_wr_req_data_i),
      .f_wr_valid_i		(gen_wr_req_ctrl_i.valid),
      .f_wr_ready_o		(gen_wr_req_ctrl_i.ready),
      .rd_req_id_o		(ft_rd_req_o.id),
      .rd_req_addr_o		(ft_rd_req_o.addr),
      .rd_req_valid_o		(ft_rd_req_o.valid),
      .rd_req_ready_i		(ft_rd_req_o.ready),
      .rd_rsp_id_i		(ft_rd_rsp_ctrl_i.id),
      .rd_rsp_data_i		(ft_rd_rsp_data_i),
      .rd_rsp_valid_i		(ft_rd_rsp_ctrl_i.valid),
      .rd_rsp_ready_o		(ft_rd_rsp_ctrl_i.ready),
      .wr_req_id_o		(ft_wr_req_ctrl_o.id),
      .wr_req_addr_o		(ft_wr_req_ctrl_o.addr),
      .wr_req_data_o		(ft_wr_req_data_o),
      .wr_req_strb_o		(ft_wr_req_ctrl_o.strb),
      .wr_req_valid_o		(ft_wr_req_ctrl_o.valid),
      .wr_req_ready_i		(ft_wr_req_ctrl_o.ready),
      .wr_rsp_id_i		(ft_wr_rsp_ctrl_i.id),
      .wr_rsp_bresp_i		(ft_wr_rsp_ctrl_i.bresp),
      .wr_rsp_valid_i		(ft_wr_rsp_ctrl_i.valid),
      .wr_rsp_ready_o		(ft_wr_rsp_ctrl_i.ready),
      // LCI.
      .lcl_fwd_wod_hdr_o	(ft_lcl_fwd_wod_o.hdr),
      .lcl_fwd_wod_pkt_size_o	(ft_lcl_fwd_wod_o.size),
      .lcl_fwd_wod_pkt_vc_o	(ft_lcl_fwd_wod_o.vc),
      .lcl_fwd_wod_pkt_valid_o	(ft_lcl_fwd_wod_o.valid),
      .lcl_fwd_wod_pkt_ready_i	(ft_lcl_fwd_wod_o.ready),
      // LCIA.
      .lcl_rsp_wod_hdr_i	(ft_lcl_rsp_wod_i.hdr),
      .lcl_rsp_wod_pkt_size_i	(ft_lcl_rsp_wod_i.size),
      .lcl_rsp_wod_pkt_vc_i	(ft_lcl_rsp_wod_i.vc),
      .lcl_rsp_wod_pkt_valid_i	(ft_lcl_rsp_wod_i.valid),
      .lcl_rsp_wod_pkt_ready_o	(ft_lcl_rsp_wod_i.ready),
      // Unlock.
      .lcl_rsp_wod_hdr_o	(ft_lcl_rsp_wod_o.hdr),
      .lcl_rsp_wod_pkt_size_o	(ft_lcl_rsp_wod_o.size),
      .lcl_rsp_wod_pkt_vc_o	(ft_lcl_rsp_wod_o.vc),
      .lcl_rsp_wod_pkt_valid_o	(ft_lcl_rsp_wod_o.valid),
      .lcl_rsp_wod_pkt_ready_i	(ft_lcl_rsp_wod_o.ready)
      );

   // Arbitrate between DC desc and FPGA thread desc.
   always_comb begin : ARB_IP_ASSIGN
      // ft rd req in.
      arb_ft_rd_req_i.id		= ft_rd_req_o.id;
      arb_ft_rd_req_i.addr		= ft_rd_req_o.addr;
      arb_ft_rd_req_i.valid		= ft_rd_req_o.valid;
      // Arb rd req op.
      arb_rd_req_o.ready		= rd_req_ready_i;
      // arb rd rsp.
      arb_rd_rsp_ctrl_i.id		= rd_rsp_id_i;
      arb_rd_rsp_data_i			= rd_rsp_data_i;
      arb_rd_rsp_ctrl_i.valid		= rd_rsp_valid_i;
      // ft rd rsp out
      arb_ft_rd_rsp_ctrl_o.ready	= ft_rd_rsp_ctrl_i.ready;
      // FT wr req.
      arb_ft_wr_req_ctrl_i.id		= ft_wr_req_ctrl_o.id;
      arb_ft_wr_req_ctrl_i.addr		= ft_wr_req_ctrl_o.addr;
      arb_ft_wr_req_data_i		= ft_wr_req_data_o;
      arb_ft_wr_req_ctrl_i.strb		= ft_wr_req_ctrl_o.strb;
      arb_ft_wr_req_ctrl_i.valid	= ft_wr_req_ctrl_o.valid;
      // Arb wr req.
      arb_wr_req_ctrl_o.ready		= wr_req_ready_i;
      // arb wr rsp.
      arb_wr_rsp_ctrl_i.id		= wr_rsp_id_i;
      arb_wr_rsp_ctrl_i.bresp		= wr_rsp_bresp_i;
      arb_wr_rsp_ctrl_i.valid		= wr_rsp_valid_i;
      // arb ft wr rsp.
      arb_ft_wr_rsp_ctrl_o.ready	= ft_wr_rsp_ctrl_i.ready;
   end : ARB_IP_ASSIGN
   ft_dc_desc_arb
     ft_dc_arb_inst
       (
	.clk			(clk),
	.reset			(reset),
	.ft_rd_req_id_i		(arb_ft_rd_req_i.id),
	.ft_rd_req_addr_i	(arb_ft_rd_req_i.addr),
	.ft_rd_req_valid_i	(arb_ft_rd_req_i.valid),
	.ft_rd_req_ready_o	(arb_ft_rd_req_i.ready),
	.dc_rd_req_id_i		(dc_rd_req_id_i),
	.dc_rd_req_addr_i	(dc_rd_req_addr_i),
	.dc_rd_req_valid_i	(dc_rd_req_valid_i),
	.dc_rd_req_ready_o	(dc_rd_req_ready_o),
	.rd_req_id_o		(arb_rd_req_o.id),
	.rd_req_addr_o		(arb_rd_req_o.addr),
	.rd_req_valid_o		(arb_rd_req_o.valid),
	.rd_req_ready_i		(arb_rd_req_o.ready),
	.rd_rsp_id_i		(arb_rd_rsp_ctrl_i.id),
	.rd_rsp_data_i		(arb_rd_rsp_data_i),
	.rd_rsp_valid_i		(arb_rd_rsp_ctrl_i.valid),
	.rd_rsp_ready_o		(arb_rd_rsp_ctrl_i.ready),
	.ft_rd_rsp_id_o		(arb_ft_rd_rsp_ctrl_o.id),
	.ft_rd_rsp_data_o	(arb_ft_rd_rsp_data_o),
	.ft_rd_rsp_valid_o	(arb_ft_rd_rsp_ctrl_o.valid),
	.ft_rd_rsp_ready_i	(arb_ft_rd_rsp_ctrl_o.ready),
	.dc_rd_rsp_id_o		(dc_rd_rsp_id_o),
	.dc_rd_rsp_data_o	(dc_rd_rsp_data_o),
	.dc_rd_rsp_valid_o	(dc_rd_rsp_valid_o),
	.dc_rd_rsp_ready_i	(dc_rd_rsp_ready_i),
	.ft_wr_req_id_i		(arb_ft_wr_req_ctrl_i.id),
	.ft_wr_req_addr_i	(arb_ft_wr_req_ctrl_i.addr),
	.ft_wr_req_data_i	(arb_ft_wr_req_data_i),
	.ft_wr_req_strb_i	(arb_ft_wr_req_ctrl_i.strb),
	.ft_wr_req_valid_i	(arb_ft_wr_req_ctrl_i.valid),
	.ft_wr_req_ready_o	(arb_ft_wr_req_ctrl_i.ready),
	.dc_wr_req_id_i		(dc_wr_req_id_i),
	.dc_wr_req_addr_i	(dc_wr_req_addr_i),
	.dc_wr_req_data_i	(dc_wr_req_data_i),
	.dc_wr_req_strb_i	(dc_wr_req_strb_i),
	.dc_wr_req_valid_i	(dc_wr_req_valid_i),
	.dc_wr_req_ready_o	(dc_wr_req_ready_o),
	.wr_req_id_o		(arb_wr_req_ctrl_o.id),
	.wr_req_addr_o		(arb_wr_req_ctrl_o.addr),
	.wr_req_data_o		(arb_wr_req_data_o),
	.wr_req_strb_o		(arb_wr_req_ctrl_o.strb),
	.wr_req_valid_o		(arb_wr_req_ctrl_o.valid),
	.wr_req_ready_i		(arb_wr_req_ctrl_o.ready),
	.wr_rsp_id_i		(arb_wr_rsp_ctrl_i.id),
	.wr_rsp_bresp_i		(arb_wr_rsp_ctrl_i.bresp),
	.wr_rsp_valid_i		(arb_wr_rsp_ctrl_i.valid),
	.wr_rsp_ready_o		(arb_wr_rsp_ctrl_i.ready),
	.ft_wr_rsp_id_o		(arb_ft_wr_rsp_ctrl_o.id),
	.ft_wr_rsp_bresp_o	(arb_ft_wr_rsp_ctrl_o.bresp),
	.ft_wr_rsp_valid_o	(arb_ft_wr_rsp_ctrl_o.valid),
	.ft_wr_rsp_ready_i	(arb_ft_wr_rsp_ctrl_o.ready),
	.dc_wr_rsp_id_o		(dc_wr_rsp_id_o),
	.dc_wr_rsp_bresp_o	(dc_wr_rsp_bresp_o),
	.dc_wr_rsp_valid_o	(dc_wr_rsp_valid_o),
	.dc_wr_rsp_ready_i	(dc_wr_rsp_ready_i)
	);

endmodule // ft_cnc_rmw_top

`endif
