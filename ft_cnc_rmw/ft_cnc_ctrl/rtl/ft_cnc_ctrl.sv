/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2023-04-14
 * Project : Enzian
 *
 * Copyright (c) 2023, ETH Zurich.  All rights reserved.
 *
 */

`ifndef FT_CNC_CTRL_SV
 `define FT_CNC_CTRL_SV

/*
 * AXI4-Lite configuration for coherent table. 
 * 
 * Address Map
 * Write 
 *   Byte Address 0  : st_un_al_addr_o
 *   Byte Address 8  : num_cls_to_rmw_o
 *   Byte address 16 : rmw_en_o
 * Read 
 *   Byte Address 0  : odd_tot_timer_i
 *   Byte Address 8  : odd_num_wr_issued_i
 *   Byte Address 16 : odd_num_rd_issued_i
 *   Byte Address 24 : even_tot_timer_i
 *   Byte Address 32 : even_num_wr_issued_i
 *   Byte Address 40 : even_num_rd_issued_i
 * 
 * Description:
 * st_un_al_addr_o - Start unaliased address. 
 * num_cls_to_rmw_o - number of cls to read modify write. 
 * rmw_en_o - read modify write enable (1 is enable, 0 is disable).
 * 
 * odd/even_tot_timer_i - timer results from odd and even threads. 
 * odd/even_num_rd_issued_i - number of reads issued by odd or even thread.
 * odd/even_num_wr_issued_i - number of writes issued by odd or even thread.
 * 
 * Note:
 *  total time of running is max(odd_tot_timer_i, even_tot_timer_i)
 *  total number of rd issued is sum(odd_num_rd_issued_i, even_num_rd_issued_i)
 *  ttoal number of wr issued is sum(odd_num_wr_issued_i, even_num_wr_issued_i)
 * 
 */

module ft_cnc_ctrl
  (
   input logic 	       clk,
   input logic 	       reset,
   // Secondary AW AXI Lite channel. 
   input logic [43:0]  s_io_axil_awaddr,
   input logic 	       s_io_axil_awvalid,
   output logic        s_io_axil_awready,
   // Secondary W AXI lite channel.
   input logic [63:0]  s_io_axil_wdata,
   input logic [7:0]   s_io_axil_wstrb,
   input logic 	       s_io_axil_wvalid,
   output logic        s_io_axil_wready,
   // Secondary Bresp AXIL channel
   output logic [1:0]  s_io_axil_bresp,
   output logic        s_io_axil_bvalid,
   input logic 	       s_io_axil_bready,
   // Secondary AR AXIL channel.
   input logic [43:0]  s_io_axil_araddr,
   input logic 	       s_io_axil_arvalid,
   output logic        s_io_axil_arready,
   // Secondary RD AXIL channel
   output logic [63:0] s_io_axil_rdata,
   output logic [1:0]  s_io_axil_rresp,
   output logic        s_io_axil_rvalid,
   input logic 	       s_io_axil_rready,

   // Control signals output.
   output logic [37:0] st_un_al_addr_o,
   output logic [37:0] num_cls_to_rmw_o,
   output logic        rmw_en_o,

   // Control signals input.
   // Total timer is maximum of odd and even tot_timer_i
   // Total number of rd and write issued
   // is the sum of odd and even num rd, wr issued.
   input logic [37:0]  odd_tot_timer_i,
   input logic [37:0]  odd_num_wr_issued_i,
   input logic [37:0]  odd_num_rd_issued_i,
   input logic [37:0]  even_tot_timer_i,
   input logic [37:0]  even_num_wr_issued_i,
   input logic [37:0]  even_num_rd_issued_i
   );
   localparam NREGS_OP = 3;
   localparam NREGS_IP = 6;
   
   // Address type.
   typedef struct      packed {
      logic [28:0]     unused;
      logic [10:0]     reg_addr; //address of 64 bit register
      logic [2:0]      byte_idx; //8 bytes per word
   } gpio_addr_t;

   // output Data type.
   // NREGS_OP 64-bit words split into 8 bytes, each byte is 8 bits.
   typedef union      packed {
      logic [NREGS_OP-1:0][7:0][7:0] data;
      logic [NREGS_OP-1:0][63:0]     flat;
   } op_data_t;

   typedef union 		     packed {
      logic [NREGS_IP-1:0][7:0][7:0] data;
      logic [NREGS_IP-1:0][63:0]     flat;
   } ip_data_t;

   enum 	       {S_WRADDR, S_WRDATA, S_WRRESP} wr_state_reg, wr_state_next;
   enum 	       {S_RDADDR, S_RDDATA} rd_state_reg, rd_state_next;
   gpio_addr_t awaddr_reg, awaddr_next;
   gpio_addr_t araddr_reg, araddr_next;
      
   // Configuration registers connected to output. 
   op_data_t gpio_op_reg, gpio_op_next;
   ip_data_t gpio_ip;

   always_comb begin : OP_ASSIGN
      s_io_axil_bresp = '0; // Always Ok.
      s_io_axil_rresp = '0; // Always ok.
   end : OP_ASSIGN

   // cast gpio_op registers to output signals. 
   always_comb begin : GPIO_OP_ASSIGN
      st_un_al_addr_o  = gpio_op_reg.flat[0][37:0];
      num_cls_to_rmw_o = gpio_op_reg.flat[1][37:0];
      rmw_en_o         = gpio_op_reg.data[2][0][0];
   end : GPIO_OP_ASSIGN

   // Read input signals into gpio_ip
   always_comb begin : GPIO_IP_ASSIGN
      // set lower 38 bits to input values.
      // rest of upper bits are set to 0.
      gpio_ip.flat[0][63:0] = '0;
      gpio_ip.flat[1][63:0] = '0;
      gpio_ip.flat[2][63:0] = '0;
      gpio_ip.flat[3][63:0] = '0;
      gpio_ip.flat[4][63:0] = '0;
      gpio_ip.flat[5][63:0] = '0;
      gpio_ip.flat[0][37:0] = odd_tot_timer_i;
      gpio_ip.flat[1][37:0] = odd_num_rd_issued_i;
      gpio_ip.flat[2][37:0] = odd_num_wr_issued_i;
      gpio_ip.flat[3][37:0] = even_tot_timer_i;
      gpio_ip.flat[4][37:0] = even_num_rd_issued_i;
      gpio_ip.flat[5][37:0] = even_num_wr_issued_i;
   end : GPIO_IP_ASSIGN


   // Write controller.
   //  Wait for valid address.
   //  then wait for valid data and write to op register
   //  then send bresp.
   always_comb begin : WR_CONTROLLER
      s_io_axil_awready = '0;
      s_io_axil_wready = '0;
      s_io_axil_bvalid = '0;
      wr_state_next = wr_state_reg;
      awaddr_next = awaddr_reg;
      gpio_op_next.data = gpio_op_reg.data;
      case(wr_state_reg)
	S_WRADDR: begin
	   s_io_axil_awready = 1'b1;
	   if(s_io_axil_awvalid & s_io_axil_awready) begin
	      awaddr_next = gpio_addr_t'(s_io_axil_awaddr);
	      wr_state_next = S_WRDATA;
	   end
	end
	S_WRDATA: begin
	   s_io_axil_wready = 1'b1;
	   if(s_io_axil_wvalid & s_io_axil_wready) begin
	      do_write(awaddr_reg, s_io_axil_wdata, s_io_axil_wstrb);
	      wr_state_next = S_WRRESP;
	   end
	end
	S_WRRESP: begin
	   s_io_axil_bvalid = 1'b1;
	   if(s_io_axil_bvalid & s_io_axil_bready) begin
	      wr_state_next = S_WRADDR;
	   end
	end
      endcase
   end : WR_CONTROLLER

   // Read controller
   //  Wait for valid address.
   //  then send read data. 
   always_comb begin : RD_CONTROLLER
      rd_state_next = rd_state_reg;
      s_io_axil_arready = 1'b0;
      s_io_axil_rvalid = 1'b0;
      araddr_next = araddr_reg;
      s_io_axil_rdata = '0;
      case(rd_state_reg)
	S_RDADDR: begin
	   s_io_axil_arready = 1'b1;
	   if(s_io_axil_arvalid & s_io_axil_arready) begin
	      araddr_next = s_io_axil_araddr;
	      rd_state_next = S_RDDATA;
	   end
	end
	S_RDDATA: begin
	   if(araddr_reg.reg_addr < NREGS_IP) begin
	      s_io_axil_rdata = gpio_ip.flat[araddr_reg.reg_addr];
	   end
	   s_io_axil_rvalid = 1'b1;
	   if(s_io_axil_rvalid & s_io_axil_rready) begin
	      rd_state_next = S_RDADDR;
	   end
	end
      endcase
   end : RD_CONTROLLER

   always_ff @(posedge clk) begin : REG_ASSIGN
      wr_state_reg <= wr_state_next;
      rd_state_reg <= rd_state_next;
      awaddr_reg  <= awaddr_next;
      araddr_reg  <= araddr_next;
      gpio_op_reg <= gpio_op_next;
      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset
	 wr_state_reg <= S_WRADDR;
	 rd_state_reg <= S_RDADDR;
	 awaddr_reg   <= '0;
	 araddr_reg   <= '0;
	 gpio_op_reg  <= '0;
      end
   end : REG_ASSIGN

   // write handling byte strobe signals. 
   task static do_write(
			input 		       gpio_addr_t addr,
			input logic [7:0][7:0] wdata,
			input logic [7:0]      wstrb
			);
      integer 				       b_idx; //byte index.
      if(addr.reg_addr < NREGS_OP) begin
	 for(b_idx = 0; b_idx < 8; b_idx=b_idx+1) begin
	    if(wstrb[b_idx] == 1'b1) begin
	       gpio_op_next.data[addr.reg_addr][b_idx] = wdata[b_idx];
	    end
	 end
      end
   endtask //do_write

endmodule // ft_cnc_ctrl

`endif

