#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
$XILINX_VIVADO/data/verilog/src/glbl.v \
../testbench/ft_cnc_ctrlTb.sv \
../rtl/ft_cnc_ctrl.sv 

xelab -debug typical -incremental -L xpm worklib.ft_cnc_ctrlTb worklib.glbl -s worklib.ft_cnc_ctrlTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.ft_cnc_ctrlTb 
xsim -gui worklib.ft_cnc_ctrlTb 
