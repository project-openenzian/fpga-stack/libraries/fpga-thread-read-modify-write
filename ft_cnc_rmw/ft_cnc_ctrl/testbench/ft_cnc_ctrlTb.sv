
module ft_cnc_ctrlTb();


   //input output ports 
   //Input signals
   logic 	       clk;
   logic 	       reset;
   logic [43:0]        s_io_axil_awaddr;
   logic 	       s_io_axil_awvalid;
   logic [63:0]        s_io_axil_wdata;
   logic [7:0] 	       s_io_axil_wstrb;
   logic 	       s_io_axil_wvalid;
   logic 	       s_io_axil_bready;
   logic [43:0]        s_io_axil_araddr;
   logic 	       s_io_axil_arvalid;
   logic 	       s_io_axil_rready;
   logic [37:0]        odd_tot_timer_i;
   logic [37:0]        odd_num_wr_issued_i;
   logic [37:0]        odd_num_rd_issued_i;
   logic [37:0]        even_tot_timer_i;
   logic [37:0]        even_num_wr_issued_i;
   logic [37:0]        even_num_rd_issued_i;

   //Output signals
   logic 	       s_io_axil_awready;
   logic 	       s_io_axil_wready;
   logic [1:0] 	       s_io_axil_bresp;
   logic 	       s_io_axil_bvalid;
   logic 	       s_io_axil_arready;
   logic [63:0]        s_io_axil_rdata;
   logic [1:0] 	       s_io_axil_rresp;
   logic 	       s_io_axil_rvalid;
   logic [37:0]        st_un_al_addr_o;
   logic [37:0]        num_cls_to_rmw_o;
   logic 	       rmw_en_o;


   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   ft_cnc_ctrl ft_cnc_ctrl1 (
			     .clk(clk),
			     .reset(reset),
			     .s_io_axil_awaddr(s_io_axil_awaddr),
			     .s_io_axil_awvalid(s_io_axil_awvalid),
			     .s_io_axil_wdata(s_io_axil_wdata),
			     .s_io_axil_wstrb(s_io_axil_wstrb),
			     .s_io_axil_wvalid(s_io_axil_wvalid),
			     .s_io_axil_bready(s_io_axil_bready),
			     .s_io_axil_araddr(s_io_axil_araddr),
			     .s_io_axil_arvalid(s_io_axil_arvalid),
			     .s_io_axil_rready(s_io_axil_rready),
			     .odd_tot_timer_i(odd_tot_timer_i),
			     .odd_num_wr_issued_i(odd_num_wr_issued_i),
			     .odd_num_rd_issued_i(odd_num_rd_issued_i),
			     .even_tot_timer_i(even_tot_timer_i),
			     .even_num_wr_issued_i(even_num_wr_issued_i),
			     .even_num_rd_issued_i(even_num_rd_issued_i),
			     .s_io_axil_awready(s_io_axil_awready),
			     .s_io_axil_wready(s_io_axil_wready),
			     .s_io_axil_bresp(s_io_axil_bresp),
			     .s_io_axil_bvalid(s_io_axil_bvalid),
			     .s_io_axil_arready(s_io_axil_arready),
			     .s_io_axil_rdata(s_io_axil_rdata),
			     .s_io_axil_rresp(s_io_axil_rresp),
			     .s_io_axil_rvalid(s_io_axil_rvalid),
			     .st_un_al_addr_o(st_un_al_addr_o),
			     .num_cls_to_rmw_o(num_cls_to_rmw_o),
			     .rmw_en_o(rmw_en_o)
			     );//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports 
      clk = '0;
      reset = '1;
      s_io_axil_awaddr = '0;
      s_io_axil_awvalid = '0;
      s_io_axil_wdata = '0;
      s_io_axil_wstrb = '0;
      s_io_axil_wvalid = '0;
      s_io_axil_bready = '0;
      s_io_axil_araddr = '0;
      s_io_axil_arvalid = '0;
      s_io_axil_rready = '0;
      odd_tot_timer_i = '0;
      odd_num_wr_issued_i = '0;
      odd_num_rd_issued_i = '0;
      even_tot_timer_i = '0;
      even_num_wr_issued_i = '0;
      even_num_rd_issued_i = '0;

      ##5;
      reset = 1'b0;
      axil_write(
		 .awaddr('0),
		 .wdata('1),
		 .wstrb(8'b01010101)
		 );
      
      axil_write(
		 .awaddr('d8), // byte address 8.
		 .wdata('1),
		 .wstrb(8'b00110011)
		 );
      
      axil_write(
		 .awaddr('d16), // byte address 16
		 .wdata('d1),
		 .wstrb(8'b00000001)
		 );
      ##5;
      odd_tot_timer_i = 'd1;
      odd_num_rd_issued_i = 'd2;
      odd_num_wr_issued_i = 'd3;
      even_tot_timer_i = 'd4;
      even_num_rd_issued_i = 'd5;
      even_num_wr_issued_i = 'd6;
      ##1;
      axil_read(.araddr('d0));
      axil_read(.araddr('d8));
      axil_read(.araddr('d16));
      axil_read(.araddr('d24));
      axil_read(.araddr('d32));
      axil_read(.araddr('d40));
      
      #500 $finish;
   end

   task static axil_read(    
			     input logic [43:0] araddr
			);
      s_io_axil_araddr = araddr;
      s_io_axil_arvalid = 1'b1;
      s_io_axil_rready = 1'b0;
      wait(s_io_axil_arvalid & s_io_axil_arready);
      ##1;
      s_io_axil_arvalid = 1'b0;
      s_io_axil_rready = 1'b1;
      wait(s_io_axil_rvalid & s_io_axil_rready);
      ##1;
      s_io_axil_rready = 1'b0;
   endtask // axil_read
   


   
   task static axil_write(    
			      input logic [43:0] awaddr,
			      input logic [63:0] wdata,
			      input logic [7:0]  wstrb
			 );
      
      s_io_axil_awaddr = awaddr;
      s_io_axil_awvalid = 1'b1;
      s_io_axil_wvalid = 1'b0;
      s_io_axil_bready = 1'b0;
      wait(s_io_axil_awvalid & s_io_axil_awready);
      ##1;
      s_io_axil_wdata = wdata;
      s_io_axil_wstrb = wstrb;
      s_io_axil_wvalid = 1'b1;
      s_io_axil_awvalid = 1'b0;
      s_io_axil_bready = 1'b0;
      wait(s_io_axil_wvalid & s_io_axil_wready);
      ##1;
      s_io_axil_wvalid = 1'b0;
      s_io_axil_awvalid = 1'b0;
      s_io_axil_bready = 1'b1;
      wait(s_io_axil_bvalid & s_io_axil_bready);
      ##1;
      s_io_axil_bready = 1'b0;
   endtask //write
endmodule
