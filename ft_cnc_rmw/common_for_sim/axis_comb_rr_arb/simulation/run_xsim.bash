#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
$XILINX_VIVADO/data/verilog/src/glbl.v \
../testbench/axis_comb_rr_arbTb.sv \
../rtl/axis_comb_rr_arb.sv 

xelab -debug typical -incremental -L xpm worklib.axis_comb_rr_arbTb worklib.glbl -s worklib.axis_comb_rr_arbTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.axis_comb_rr_arbTb 
xsim -gui worklib.axis_comb_rr_arbTb 
