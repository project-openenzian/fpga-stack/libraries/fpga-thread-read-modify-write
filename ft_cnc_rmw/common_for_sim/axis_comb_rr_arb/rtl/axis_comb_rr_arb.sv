/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2022-10-10
 * Project : Enzian
 *
 * Copyright (c) 2022, ETH Zurich.  All rights reserved.
 *
 */

`ifndef AXIS_COMB_RR_ARB_SV
`define AXIS_COMB_RR_ARB_SV

/*
 * Module Description:
 *  N to 1 Roundrobin mux. 
 *  output is chosen based on round robin arbitration. 
 *  Fully combinational.
 *
 * Input Output Description:
 *  N input channels with data, valid ready flow control. 
 *  1 output channel with data, valid ready flow control. 
 *  
 *
 * Architecture Description:
 *  Generate select signal based on round robin register. 
 *  Assign downstream data based on select signal.
 *
 * Modifiable Parameters:
 *  ID_WIDTH - width of ID sent along with data. 
 *  DATA_WIDTH - width of data in input and output channels. 
 *  NUM_IN - Number of input channels. 
 *
 * Non-modifiable Parameters:
 *  None.
 * 
 * Modules Used:
 *  None.
 *
 */

module axis_comb_rr_arb #
  (
   parameter ID_WIDTH = 5,
   parameter DATA_WIDTH = 64,
   parameter NUM_IN = 4
   )
   (
    // clk is needed for rr register updates. 
    input logic 			     clk,
    input logic 			     reset,
    
    input logic [NUM_IN-1:0][ID_WIDTH-1:0]   us_id_i,
    input logic [NUM_IN-1:0][DATA_WIDTH-1:0] us_data_i,
    input logic [NUM_IN-1:0] 		     us_valid_i,
    output logic [NUM_IN-1:0] 		     us_ready_o,

    output logic [ID_WIDTH-1:0] 	     ds_id_o,
    output logic [DATA_WIDTH-1:0] 	     ds_data_o,
    output logic 			     ds_valid_o,
    input logic 			     ds_ready_i
    );
   localparam SEL_WIDTH = $clog2(NUM_IN);

   logic [SEL_WIDTH-1:0] rr_reg = '0, rr_next;
   logic [SEL_WIDTH-1:0] curr_sel;
   
   always_comb begin : DATA_PATH
      ds_valid_o = 1'b0;
      us_ready_o = '0;
      curr_sel = rr_reg;
      for( integer i=0; i<NUM_IN; i=i+1 ) begin
	 if(i+rr_reg >= NUM_IN) begin
	    if(us_valid_i[i+rr_reg-NUM_IN]) begin
	       ds_valid_o = us_valid_i[i+rr_reg-NUM_IN];
	       curr_sel = i+rr_reg-NUM_IN;
	       break;
	    end
	 end else begin
	    if(us_valid_i[i+rr_reg]) begin
	       ds_valid_o = us_valid_i[i+rr_reg];
	       curr_sel = i+rr_reg;
	       break;
	    end
	 end
      end
      us_ready_o[curr_sel] = ds_ready_i;
      ds_id_o = us_id_i[curr_sel];
      ds_data_o = us_data_i[curr_sel];
   end : DATA_PATH

   always_comb begin : CONTROLLER
      rr_next = rr_reg;
      if(ds_valid_o & ds_ready_i) begin
	 rr_next = rr_reg + 1;
      end
   end : CONTROLLER

   
   always_ff @(posedge clk) begin : REG_ASSIGN
      rr_reg	<= rr_next;
      if( reset ) begin
	 // Make sure unnecessary datapath registers are not reset 
	 rr_reg	<= '0;
      end
   end : REG_ASSIGN

endmodule // axis_comb_rr_arb

`endif
