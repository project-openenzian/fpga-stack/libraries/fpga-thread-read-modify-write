import eci_cmd_defs::*;
import eci_dcs_defs::*;

module ft_dc_desc_arbTb();


   //input output ports 
   //Input signals
   logic 				clk;
   logic 				reset;
   logic [5-1:0] 			ft_rd_req_id_i;
   logic [DS_ADDR_WIDTH-1:0] 		ft_rd_req_addr_i;
   logic 				ft_rd_req_valid_i;
   logic [MAX_DCU_ID_WIDTH-1:0] 	dc_rd_req_id_i;
   logic [DS_ADDR_WIDTH-1:0] 		dc_rd_req_addr_i;
   logic 				dc_rd_req_valid_i;
   logic 				rd_req_ready_i;
   logic [MAX_DCU_ID_WIDTH-1:0] 	rd_rsp_id_i;
   logic [ECI_CL_WIDTH-1:0] 		rd_rsp_data_i;
   logic 				rd_rsp_valid_i;
   logic 				ft_rd_rsp_ready_i;
   logic 				dc_rd_rsp_ready_i;
   logic [5-1:0] 			ft_wr_req_id_i;
   logic [DS_ADDR_WIDTH-1:0] 		ft_wr_req_addr_i;
   logic [ECI_CL_WIDTH-1:0] 		ft_wr_req_data_i;
   logic [ECI_CL_SIZE_BYTES-1:0] 	ft_wr_req_strb_i;
   logic 				ft_wr_req_valid_i;
   logic [MAX_DCU_ID_WIDTH-1:0] 	dc_wr_req_id_i;
   logic [DS_ADDR_WIDTH-1:0] 		dc_wr_req_addr_i;
   logic [ECI_CL_WIDTH-1:0] 		dc_wr_req_data_i;
   logic [ECI_CL_SIZE_BYTES-1:0] 	dc_wr_req_strb_i;
   logic 				dc_wr_req_valid_i;
   logic 				wr_req_ready_i;
   logic [MAX_DCU_ID_WIDTH-1:0] 	wr_rsp_id_i;
   logic [1:0] 				wr_rsp_bresp_i;
   logic 				wr_rsp_valid_i;
   logic 				ft_wr_rsp_ready_i;
   logic 				dc_wr_rsp_ready_i;

   //Output signals
   logic 				ft_rd_req_ready_o;
   logic 				dc_rd_req_ready_o;
   logic [MAX_DCU_ID_WIDTH-1:0] 	rd_req_id_o;
   logic [DS_ADDR_WIDTH-1:0] 		rd_req_addr_o;
   logic 				rd_req_valid_o;
   logic 				rd_rsp_ready_o;
   logic [5-1:0] 			ft_rd_rsp_id_o;
   logic [ECI_CL_WIDTH-1:0] 		ft_rd_rsp_data_o;
   logic 				ft_rd_rsp_valid_o;
   logic [MAX_DCU_ID_WIDTH-1:0] 	dc_rd_rsp_id_o;
   logic [ECI_CL_WIDTH-1:0] 		dc_rd_rsp_data_o;
   logic 				dc_rd_rsp_valid_o;
   logic 				ft_wr_req_ready_o;
   logic 				dc_wr_req_ready_o;
   logic [MAX_DCU_ID_WIDTH-1:0] 	wr_req_id_o;
   logic [DS_ADDR_WIDTH-1:0] 		wr_req_addr_o;
   logic [ECI_CL_WIDTH-1:0] 		wr_req_data_o;
   logic [ECI_CL_SIZE_BYTES-1:0] 	wr_req_strb_o;
   logic 				wr_req_valid_o;
   logic 				wr_rsp_ready_o;
   logic [5-1:0] 			ft_wr_rsp_id_o;
   logic [1:0] 				ft_wr_rsp_bresp_o;
   logic 				ft_wr_rsp_valid_o;
   logic [MAX_DCU_ID_WIDTH-1:0] 	dc_wr_rsp_id_o;
   logic [1:0] 				dc_wr_rsp_bresp_o;
   logic 				dc_wr_rsp_valid_o;


   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   ft_dc_desc_arb ft_dc_desc_arb1 (
				   .clk(clk),
				   .reset(reset),
				   .ft_rd_req_id_i(ft_rd_req_id_i),
				   .ft_rd_req_addr_i(ft_rd_req_addr_i),
				   .ft_rd_req_valid_i(ft_rd_req_valid_i),
				   .dc_rd_req_id_i(dc_rd_req_id_i),
				   .dc_rd_req_addr_i(dc_rd_req_addr_i),
				   .dc_rd_req_valid_i(dc_rd_req_valid_i),
				   .rd_req_ready_i(rd_req_ready_i),
				   .rd_rsp_id_i(rd_rsp_id_i),
				   .rd_rsp_data_i(rd_rsp_data_i),
				   .rd_rsp_valid_i(rd_rsp_valid_i),
				   .ft_rd_rsp_ready_i(ft_rd_rsp_ready_i),
				   .dc_rd_rsp_ready_i(dc_rd_rsp_ready_i),
				   .ft_wr_req_id_i(ft_wr_req_id_i),
				   .ft_wr_req_addr_i(ft_wr_req_addr_i),
				   .ft_wr_req_data_i(ft_wr_req_data_i),
				   .ft_wr_req_strb_i(ft_wr_req_strb_i),
				   .ft_wr_req_valid_i(ft_wr_req_valid_i),
				   .dc_wr_req_id_i(dc_wr_req_id_i),
				   .dc_wr_req_addr_i(dc_wr_req_addr_i),
				   .dc_wr_req_data_i(dc_wr_req_data_i),
				   .dc_wr_req_strb_i(dc_wr_req_strb_i),
				   .dc_wr_req_valid_i(dc_wr_req_valid_i),
				   .wr_req_ready_i(wr_req_ready_i),
				   .wr_rsp_id_i(wr_rsp_id_i),
				   .wr_rsp_bresp_i(wr_rsp_bresp_i),
				   .wr_rsp_valid_i(wr_rsp_valid_i),
				   .ft_wr_rsp_ready_i(ft_wr_rsp_ready_i),
				   .dc_wr_rsp_ready_i(dc_wr_rsp_ready_i),
				   .ft_rd_req_ready_o(ft_rd_req_ready_o),
				   .dc_rd_req_ready_o(dc_rd_req_ready_o),
				   .rd_req_id_o(rd_req_id_o),
				   .rd_req_addr_o(rd_req_addr_o),
				   .rd_req_valid_o(rd_req_valid_o),
				   .rd_rsp_ready_o(rd_rsp_ready_o),
				   .ft_rd_rsp_id_o(ft_rd_rsp_id_o),
				   .ft_rd_rsp_data_o(ft_rd_rsp_data_o),
				   .ft_rd_rsp_valid_o(ft_rd_rsp_valid_o),
				   .dc_rd_rsp_id_o(dc_rd_rsp_id_o),
				   .dc_rd_rsp_data_o(dc_rd_rsp_data_o),
				   .dc_rd_rsp_valid_o(dc_rd_rsp_valid_o),
				   .ft_wr_req_ready_o(ft_wr_req_ready_o),
				   .dc_wr_req_ready_o(dc_wr_req_ready_o),
				   .wr_req_id_o(wr_req_id_o),
				   .wr_req_addr_o(wr_req_addr_o),
				   .wr_req_data_o(wr_req_data_o),
				   .wr_req_strb_o(wr_req_strb_o),
				   .wr_req_valid_o(wr_req_valid_o),
				   .wr_rsp_ready_o(wr_rsp_ready_o),
				   .ft_wr_rsp_id_o(ft_wr_rsp_id_o),
				   .ft_wr_rsp_bresp_o(ft_wr_rsp_bresp_o),
				   .ft_wr_rsp_valid_o(ft_wr_rsp_valid_o),
				   .dc_wr_rsp_id_o(dc_wr_rsp_id_o),
				   .dc_wr_rsp_bresp_o(dc_wr_rsp_bresp_o),
				   .dc_wr_rsp_valid_o(dc_wr_rsp_valid_o)
				   );//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports 
      clk = '0;
      reset = '0;
      ft_rd_req_id_i = '0;
      ft_rd_req_addr_i = '0;
      ft_rd_req_valid_i = '0;
      dc_rd_req_id_i = '0;
      dc_rd_req_addr_i = '0;
      dc_rd_req_valid_i = '0;
      //rd_req_ready_i = '0;
      //rd_rsp_id_i = '0;
      //rd_rsp_data_i = '0;
      //rd_rsp_valid_i = '0;
      ft_rd_rsp_ready_i = '0;
      dc_rd_rsp_ready_i = '0;
      ft_wr_req_id_i = '0;
      ft_wr_req_addr_i = '0;
      ft_wr_req_data_i = '0;
      ft_wr_req_strb_i = '0;
      ft_wr_req_valid_i = '0;
      dc_wr_req_id_i = '0;
      dc_wr_req_addr_i = '0;
      dc_wr_req_data_i = '0;
      dc_wr_req_strb_i = '0;
      dc_wr_req_valid_i = '0;
      //wr_req_ready_i = '0;
      //wr_rsp_id_i = '0;
      //wr_rsp_bresp_i = '0;
      //wr_rsp_valid_i = '0;
      ft_wr_rsp_ready_i = '0;
      dc_wr_rsp_ready_i = '0;

      ##5;
      reset = 1'b0;
      ft_rd_rsp_ready_i = 1'b1;
      dc_rd_rsp_ready_i = 1'b1;
      ft_wr_rsp_ready_i = 1'b1;
      dc_wr_rsp_ready_i = 1'b1;
      
      //ft_rd_req_id_i = 'd1;
      //ft_rd_req_addr_i = 'd128;
      //ft_rd_req_valid_i = 1'b1;
      //dc_rd_req_id_i = 'd1;
      //dc_rd_req_addr_i = 'd256;
      //dc_rd_req_valid_i = 1'b1;

      fpga_thr_wr(
		  .id_i('1),
		  .addr_i('d128),
		  .data_i('d1024)
		  );

      dc_wr(
	    .id_i(7'b0111111),
	    .addr_i('d256),
	    .data_i('d2048)
	    );


      fpga_thr_rd(
		  .id_i('1),
		  .addr_i('d128)
		  );

      dc_rd(
	    .id_i(7'b0111111),
	    .addr_i('d128)
	    );
      
      #500 $finish;
   end // initial begin

   task static fpga_thr_wr(
			   input logic [5-1:0] 		   id_i,
			   input logic [DS_ADDR_WIDTH-1:0] addr_i,
			   input logic [ECI_CL_WIDTH-1:0]  data_i
			   );
      ft_wr_req_id_i = id_i;
      ft_wr_req_addr_i = addr_i;
      ft_wr_req_data_i = data_i;
      ft_wr_req_strb_i = '1;
      ft_wr_req_valid_i = 1'b1;
      wait(ft_wr_req_valid_i & ft_wr_req_ready_o);
      ##1;
      ft_wr_req_valid_i = 1'b0;
   endtask //FPGA_THR_WR

   task static dc_wr(
		     input logic [MAX_DCU_ID_WIDTH-1:0] id_i,
		     input logic [DS_ADDR_WIDTH-1:0] 	addr_i,
		     input logic [ECI_CL_WIDTH-1:0] 	data_i
		     );
      dc_wr_req_id_i = id_i;
      dc_wr_req_addr_i = addr_i;
      dc_wr_req_data_i = data_i;
      dc_wr_req_strb_i = '1;
      dc_wr_req_valid_i = 1'b1;
      wait(dc_wr_req_valid_i & dc_wr_req_ready_o);
      ##1;
      dc_wr_req_valid_i = 1'b0;
   endtask //dc_wr

   task static fpga_thr_rd(
			   input logic [5-1:0] 		   id_i,
			   input logic [DS_ADDR_WIDTH-1:0] addr_i			   
			   );
      ft_rd_req_id_i = id_i;
      ft_rd_req_addr_i = addr_i;
      ft_rd_req_valid_i = 1'b1;
      wait(ft_rd_req_valid_i & ft_rd_req_ready_o);
      ##1;
      ft_rd_req_valid_i = 1'b0;
   endtask //fpga_thr_rd

   task static dc_rd(
		     input logic [MAX_DCU_ID_WIDTH-1:0] id_i,
		     input logic [DS_ADDR_WIDTH-1:0] 	addr_i			   
		     );
      dc_rd_req_id_i = id_i;
      dc_rd_req_addr_i = addr_i;
      dc_rd_req_valid_i = 1'b1;
      wait(dc_rd_req_valid_i & dc_rd_req_ready_o);
      ##1;
      dc_rd_req_valid_i = 1'b0;
   endtask //fpga_thr_rd

   // This is word addressable memory.
   // An address points to a CL.
   // Give CL index as input to this module.
   // It can hold 2^10 CLs. (only for sim).
   word_addr_mem #
     (
      .ID_WIDTH(MAX_DCU_ID_WIDTH),
      .ADDR_WIDTH(10),
      .DATA_WIDTH(ECI_CL_WIDTH)
       )
   mem_inst_1
     (
      .clk(clk),
      .reset(reset),

      .rd_req_id_i(rd_req_id_o),
      .rd_req_addr_i(rd_req_addr_o[9+7:7]),
      .rd_req_valid_i(rd_req_valid_o),
      .rd_req_ready_o(rd_req_ready_i),

      .wr_req_id_i(wr_req_id_o),
      .wr_req_addr_i(wr_req_addr_o[9+7:7]),
      .wr_req_data_i(wr_req_data_o),
      .wr_req_valid_i(wr_req_valid_o),
      .wr_req_ready_o(wr_req_ready_i),

      .rd_rsp_id_o(rd_rsp_id_i),
      .rd_rsp_data_o(rd_rsp_data_i),
      .rd_rsp_valid_o(rd_rsp_valid_i),
      .rd_rsp_ready_i(rd_rsp_ready_o),

      .wr_rsp_id_o(wr_rsp_id_i),
      .wr_rsp_bresp_o(wr_rsp_bresp_i),
      .wr_rsp_valid_o(wr_rsp_valid_i),
      .wr_rsp_ready_i(wr_rsp_ready_o)
      );

   
endmodule
