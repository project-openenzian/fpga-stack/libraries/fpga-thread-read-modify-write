#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
      $XILINX_VIVADO/data/verilog/src/glbl.v \
      ../rtl/eci_cmd_defs.sv \
      ../rtl/eci_dcs_defs.sv \
      ../testbench/ft_dc_desc_arbTb.sv \
      ../rtl/ft_dc_desc_arb.sv \
      ../rtl/axis_2_router.sv \
      ../rtl/axis_comb_rr_arb.sv \
      ../rtl/axis_pipeline_stage.sv \
      ../testbench/word_addr_mem.sv 

xelab -debug typical -incremental -L xpm worklib.ft_dc_desc_arbTb worklib.glbl -s worklib.ft_dc_desc_arbTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.ft_dc_desc_arbTb 
xsim -gui worklib.ft_dc_desc_arbTb 
