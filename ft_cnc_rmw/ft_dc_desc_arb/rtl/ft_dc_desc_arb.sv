/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2023-02-25
 * Project : Enzian
 *
 * Copyright (c) 2023, ETH Zurich.  All rights reserved.
 *
 */

`ifndef FT_DC_DESC_ARB_SV
 `define FT_DC_DESC_ARB_SV

import eci_cmd_defs::*;
import eci_dcs_defs::*;

// Arbitrate between read and write
// requests between DC and FPGA threads
// to issue one request to the memory.
//
// Responses from memory are routed to
// either DC or FPGA thread using the ID.
//
// FPGA IDs are 5 bits, and DC IDs are 7 bits.
// with DS_NUM_SETS_PER_DCU == 128, we have
// MSB of ID issued from DC to be always 0.
// we convert the 5 bit FPGA thread ID into 7 bits
// with MSB set to 1 and rest to 0 before
// sending requests.
//
// when responses arrive the MSB is used to
// route to DC or FPGA threads.
// if MSB of response ID is 1, route to FPGA
// threads else route to DC. 

module ft_dc_desc_arb 
  (
   input logic 				clk,
   input logic 				reset,

   // Input FPGA thread read descriptors. 
   input logic [5-1:0] 			ft_rd_req_id_i,
   input logic [DS_ADDR_WIDTH-1:0] 	ft_rd_req_addr_i,
   input logic 				ft_rd_req_valid_i,
   output logic 			ft_rd_req_ready_o,

   // Input DC read descriptors.
   input logic [MAX_DCU_ID_WIDTH-1:0] 	dc_rd_req_id_i,
   input logic [DS_ADDR_WIDTH-1:0] 	dc_rd_req_addr_i,
   input logic 				dc_rd_req_valid_i,
   output logic 			dc_rd_req_ready_o,

   // Output arbitrated read descriptor to mem.
   output logic [MAX_DCU_ID_WIDTH-1:0] 	rd_req_id_o,
   output logic [DS_ADDR_WIDTH-1:0] 	rd_req_addr_o,
   output logic 			rd_req_valid_o,
   input logic 				rd_req_ready_i,

   // Input read response from mem.
   input logic [MAX_DCU_ID_WIDTH-1:0] 	rd_rsp_id_i,
   input logic [ECI_CL_WIDTH-1:0] 	rd_rsp_data_i,
   input logic 				rd_rsp_valid_i,
   output logic 			rd_rsp_ready_o,

   // Output rd response to FPGA threads.
   output logic [5-1:0] 		ft_rd_rsp_id_o,
   output logic [ECI_CL_WIDTH-1:0] 	ft_rd_rsp_data_o,
   output logic 			ft_rd_rsp_valid_o,
   input logic 				ft_rd_rsp_ready_i,

   // Output rd response to DC.
   output logic [MAX_DCU_ID_WIDTH-1:0] 	dc_rd_rsp_id_o,
   output logic [ECI_CL_WIDTH-1:0] 	dc_rd_rsp_data_o,
   output logic 			dc_rd_rsp_valid_o,
   input logic 				dc_rd_rsp_ready_i,

   // Input write request from FPGA threads.
   input logic [5-1:0] 			ft_wr_req_id_i,
   input logic [DS_ADDR_WIDTH-1:0] 	ft_wr_req_addr_i,
   input logic [ECI_CL_WIDTH-1:0] 	ft_wr_req_data_i,
   input logic [ECI_CL_SIZE_BYTES-1:0] 	ft_wr_req_strb_i, 
   input logic 				ft_wr_req_valid_i,
   output logic 			ft_wr_req_ready_o,
   
   // Input write request desc from DC.
   input logic [MAX_DCU_ID_WIDTH-1:0] 	dc_wr_req_id_i,
   input logic [DS_ADDR_WIDTH-1:0] 	dc_wr_req_addr_i,
   input logic [ECI_CL_WIDTH-1:0] 	dc_wr_req_data_i,
   input logic [ECI_CL_SIZE_BYTES-1:0] 	dc_wr_req_strb_i, 
   input logic 				dc_wr_req_valid_i,
   output logic 			dc_wr_req_ready_o,

   // Output write request to memory.
   output logic [MAX_DCU_ID_WIDTH-1:0] 	wr_req_id_o,
   output logic [DS_ADDR_WIDTH-1:0] 	wr_req_addr_o,
   output logic [ECI_CL_WIDTH-1:0] 	wr_req_data_o,
   output logic [ECI_CL_SIZE_BYTES-1:0] wr_req_strb_o, 
   output logic 			wr_req_valid_o,
   input logic 				wr_req_ready_i,

   // Input write response from memory.
   input logic [MAX_DCU_ID_WIDTH-1:0] 	wr_rsp_id_i,
   input logic [1:0] 			wr_rsp_bresp_i,
   input logic 				wr_rsp_valid_i,
   output logic 			wr_rsp_ready_o,

   // output wr rsp to FPGA threads.
   output logic [5-1:0] 		ft_wr_rsp_id_o,
   output logic [1:0] 			ft_wr_rsp_bresp_o, // not used.
   output logic 			ft_wr_rsp_valid_o,
   input logic 				ft_wr_rsp_ready_i,
   
   // Output wr rsp to DC.
   output logic [MAX_DCU_ID_WIDTH-1:0] 	dc_wr_rsp_id_o,
   output logic [1:0] 			dc_wr_rsp_bresp_o,
   output logic 			dc_wr_rsp_valid_o,
   input logic 				dc_wr_rsp_ready_i
   );

   localparam RDR_RTR_DATA_WIDTH = MAX_DCU_ID_WIDTH + ECI_CL_WIDTH;
   localparam WRRQ_ARB_DATA_WIDTH = DS_ADDR_WIDTH + ECI_CL_WIDTH + ECI_CL_SIZE_BYTES;
   localparam WRRQ_PS_DATA_WIDTH  = MAX_DCU_ID_WIDTH + WRRQ_ARB_DATA_WIDTH;
   localparam WRRS_RTR_DATA_WIDTH = MAX_DCU_ID_WIDTH + 2; //bRESP is 2 bits. 
   
   // Cast ID from FPGA thread to MAX_DCU_id_width
   logic [MAX_DCU_ID_WIDTH-1:0]        ft_rd_req_id_c_i;
   logic [DS_ADDR_WIDTH-1:0] 	       ft_rd_req_addr_c_i;
   logic 			       ft_rd_req_valid_c_i;
   logic 			       ft_rd_req_ready_c_o;
   // Rd desc arb signals.
   logic [2-1:0][MAX_DCU_ID_WIDTH-1:0] rdd_arb_us_id_i;
   logic [2-1:0][DS_ADDR_WIDTH-1:0]    rdd_arb_us_data_i;
   logic [2-1:0] 		       rdd_arb_us_valid_i;
   logic [2-1:0] 		       rdd_arb_us_ready_o;
   logic [MAX_DCU_ID_WIDTH-1:0]        rdd_arb_ds_id_o;
   logic [DS_ADDR_WIDTH-1:0] 	       rdd_arb_ds_data_o;
   logic 			       rdd_arb_ds_valid_o;
   logic 			       rdd_arb_ds_ready_i;
   // Rd rsp router singals.
   logic [RDR_RTR_DATA_WIDTH-1:0]      rdr_rtr_us_data_i;
   logic 			       rdr_rtr_us_sel_down_upbar_i;
   logic 			       rdr_rtr_us_valid_i;
   logic 			       rdr_rtr_us_ready_o;
   logic [RDR_RTR_DATA_WIDTH-1:0]      rdr_rtr_ds_up_data_o;
   logic 			       rdr_rtr_ds_up_valid_o;
   logic 			       rdr_rtr_ds_up_ready_i;
   logic [RDR_RTR_DATA_WIDTH-1:0]      rdr_rtr_ds_down_data_o;
   logic 			       rdr_rtr_ds_down_valid_o;
   logic 			       rdr_rtr_ds_down_ready_i;
   // Wr req desc arb signals.
   logic [2-1:0][MAX_DCU_ID_WIDTH-1:0] wrrq_arb_us_id_i;
   logic [2-1:0][WRRQ_ARB_DATA_WIDTH-1:0] wrrq_arb_us_data_i;
   logic [2-1:0] 			 wrrq_arb_us_valid_i;
   logic [2-1:0] 			 wrrq_arb_us_ready_o;
   logic [MAX_DCU_ID_WIDTH-1:0] 	 wrrq_arb_ds_id_o;
   logic [WRRQ_ARB_DATA_WIDTH-1:0] 	 wrrq_arb_ds_data_o;
   logic 				 wrrq_arb_ds_valid_o;
   logic 				 wrrq_arb_ds_ready_i;
   // Cat ID from FPGA thread for write req to MAX_DCU_ID_WIDTH.
   logic [MAX_DCU_ID_WIDTH-1:0] 	 ft_wr_req_id_c_i;
   logic [DS_ADDR_WIDTH-1:0] 		 ft_wr_req_addr_c_i;
   logic [ECI_CL_WIDTH-1:0] 		 ft_wr_req_data_c_i;
   logic [ECI_CL_SIZE_BYTES-1:0] 	 ft_wr_req_strb_c_i; 
   logic 				 ft_wr_req_valid_c_i;
   logic 				 ft_wr_req_ready_c_o;
   // Wr req desc pipe stage signals.
   logic [WRRQ_PS_DATA_WIDTH-1:0] 	 wrrq_ps_us_data;
   logic 				 wrrq_ps_us_valid;
   logic 				 wrrq_ps_us_ready;
   // Outgoing downstream data 
   logic [WRRQ_PS_DATA_WIDTH-1:0] 	 wrrq_ps_ds_data;
   logic 				 wrrq_ps_ds_valid;
   logic 				 wrrq_ps_ds_ready;
   // write rsp router signals.
   logic [WRRS_RTR_DATA_WIDTH-1:0] 	 wrrs_us_data_i;
   logic 				 wrrs_us_sel_down_upbar_i;
   logic 				 wrrs_us_valid_i;
   logic 				 wrrs_us_ready_o;
   logic [WRRS_RTR_DATA_WIDTH-1:0] 	 wrrs_ds_up_data_o;
   logic 				 wrrs_ds_up_valid_o;
   logic 				 wrrs_ds_up_ready_i;
   logic [WRRS_RTR_DATA_WIDTH-1:0] 	 wrrs_ds_down_data_o;
   logic 				 wrrs_ds_down_valid_o;
   logic 				 wrrs_ds_down_ready_i;
   

   // with DS_NUM_DCUS_PER_SET at 128, the MSB of IDs from
   // the DC will be 0. Here, we set the MSB of IDs from the
   // FPGA threads as 1 and this MSB is used to route to either
   // DC or FPGA thread.
   //
   // WHen DS_NUM_SETs_PER_DCU is not 128, the MSB cannot be used
   // to differentiate between DC and FPGA threads. 
   initial begin
      if(DS_NUM_SETS_PER_DCU < 128) begin
	 $error("Error: DS_NUM_SETS_PER_DCU has to be 128/256 for instance %m to work.");
	 $finish;
      end
   end

   always_comb begin : OP_ASSIGN
      ft_rd_req_ready_o = ft_rd_req_ready_c_o;
      dc_rd_req_ready_o = rdd_arb_us_ready_o[0];
      // Read desc output signals. 
      rd_req_id_o = rdd_arb_ds_id_o;
      rd_req_addr_o = rdd_arb_ds_data_o;
      rd_req_valid_o = rdd_arb_ds_valid_o;
      // Read resp from mem
      rd_rsp_ready_o = rdr_rtr_us_ready_o;
      // rd rsp to DC.
      {dc_rd_rsp_id_o, dc_rd_rsp_data_o} = rdr_rtr_ds_up_data_o;
      dc_rd_rsp_valid_o = rdr_rtr_ds_up_valid_o;
      // rd rsp to FPGA threads.
      // Ignore 2 MSB bits as ft_rd_rsp_id is only 5 bits. 
      {ft_rd_rsp_id_o, ft_rd_rsp_data_o} = rdr_rtr_ds_down_data_o[RDR_RTR_DATA_WIDTH-3:0];
      ft_rd_rsp_valid_o = rdr_rtr_ds_down_valid_o;
      // wr req DC
      dc_wr_req_ready_o = wrrq_arb_us_ready_o[0];
      // wr req FPGA threads.
      ft_wr_req_ready_o = ft_wr_req_ready_c_o;
      // wr req to mem.
      {
       wr_req_id_o,
       wr_req_addr_o,
       wr_req_data_o,
       wr_req_strb_o
       } = wrrq_ps_ds_data;
      wr_req_valid_o = wrrq_ps_ds_valid;
      // wr rsp from mem.
      wr_rsp_ready_o = wrrs_us_ready_o;
      // wr rsp to DC.
      {
       dc_wr_rsp_id_o,
       dc_wr_rsp_bresp_o
       } = wrrs_ds_up_data_o;
      dc_wr_rsp_valid_o = wrrs_ds_up_valid_o;
      // wr rsp to FPGA threads.
      // ft wr rsp is only 5 bits, ignore MSB 2 bits.
      {
       ft_wr_rsp_id_o,
       ft_wr_rsp_bresp_o
       } = wrrs_ds_down_data_o[WRRS_RTR_DATA_WIDTH-3:0]; 
      ft_wr_rsp_valid_o = wrrs_ds_down_valid_o;
   end : OP_ASSIGN

   // Set MSB of ID from FPGA thread to 1 to route to it. 
   always_comb begin : CAST_FT_ID_TO_MAX_DCU_ID
      ft_rd_req_id_c_i = {2'b10, ft_rd_req_id_i};
      ft_rd_req_addr_c_i = ft_rd_req_addr_i;
      ft_rd_req_valid_c_i = ft_rd_req_valid_i;
      ft_rd_req_ready_c_o = rdd_arb_us_ready_o[1];
   end : CAST_FT_ID_TO_MAX_DCU_ID

   // Arbitrate between read desc from
   // DC and FPGA thread, and send to output rd desc.
   // round robin arbitration.
   always_comb begin : RDD_ARB_IP_ASSIGN
      rdd_arb_us_id_i[0] = dc_rd_req_id_i;
      rdd_arb_us_data_i[0] = dc_rd_req_addr_i;
      rdd_arb_us_valid_i[0] = dc_rd_req_valid_i;

      rdd_arb_us_id_i[1] = ft_rd_req_id_c_i;
      rdd_arb_us_data_i[1] = ft_rd_req_addr_c_i;
      rdd_arb_us_valid_i[1] = ft_rd_req_valid_c_i;

      rdd_arb_ds_ready_i = rd_req_ready_i;
   end : RDD_ARB_IP_ASSIGN
   axis_comb_rr_arb #
     (
      .ID_WIDTH(MAX_DCU_ID_WIDTH),
      .DATA_WIDTH(DS_ADDR_WIDTH),
      .NUM_IN(2)
       )
   rd_desc_rr_arb_1
     (
      .clk(clk),
      .reset(reset),
      .us_id_i(rdd_arb_us_id_i),
      .us_data_i(rdd_arb_us_data_i),
      .us_valid_i(rdd_arb_us_valid_i),
      .us_ready_o(rdd_arb_us_ready_o),
      .ds_id_o(rdd_arb_ds_id_o),
      .ds_data_o(rdd_arb_ds_data_o),
      .ds_valid_o(rdd_arb_ds_valid_o),
      .ds_ready_i(rdd_arb_ds_ready_i)
      );
   // No pipeline stage as both inputs are
   // pipeline registered.


   // Route read response from mem to
   // FPGA thread or DC depending on MSB.
   // MSB == 0 ? route to DC :  route to FPGA thread.
   // There is a registered pipeline within the router
   // so no need to register the outputs.
   always_comb begin : RDR_RTR_IP_ASSIGN
      rdr_rtr_us_data_i = {rd_rsp_id_i, rd_rsp_data_i};
      rdr_rtr_us_sel_down_upbar_i = rd_rsp_id_i[MAX_DCU_ID_WIDTH-1];
      rdr_rtr_us_valid_i = rd_rsp_valid_i;
      rdr_rtr_ds_up_ready_i = dc_rd_rsp_ready_i;
      rdr_rtr_ds_down_ready_i = ft_rd_rsp_ready_i;
   end : RDR_RTR_IP_ASSIGN
   axis_2_router #
     (
      .DATA_WIDTH(RDR_RTR_DATA_WIDTH)
      )
   rdr_rtr_1
     (
      .clk(clk),
      .reset(reset),
      .us_data_i(rdr_rtr_us_data_i),
      // 1 selects down channel; 0 selects up channel. 
      .us_sel_down_upbar_i(rdr_rtr_us_sel_down_upbar_i),
      .us_valid_i(rdr_rtr_us_valid_i),
      .us_ready_o(rdr_rtr_us_ready_o),
      // up channel send to DC.
      .ds_up_data_o(rdr_rtr_ds_up_data_o),
      .ds_up_valid_o(rdr_rtr_ds_up_valid_o),
      .ds_up_ready_i(rdr_rtr_ds_up_ready_i),
      // Down channel send to FPGA threads.
      .ds_down_data_o(rdr_rtr_ds_down_data_o),
      .ds_down_valid_o(rdr_rtr_ds_down_valid_o),
      .ds_down_ready_i(rdr_rtr_ds_down_ready_i)
      );


   // Prepend 1 to ID of wr req from FPGA thread
   // to route response correctly.
   always_comb begin : CAST_FT_ID_WRRQ
      ft_wr_req_id_c_i    = {2'b10, ft_wr_req_id_i};
      ft_wr_req_addr_c_i  = ft_wr_req_addr_i;
      ft_wr_req_data_c_i  = ft_wr_req_data_i;
      ft_wr_req_strb_c_i  = ft_wr_req_strb_i;
      ft_wr_req_valid_c_i = ft_wr_req_valid_i;
      ft_wr_req_ready_c_o = wrrq_arb_us_ready_o[1];
   end : CAST_FT_ID_WRRQ

   
   // Arbitrate between write requests from
   // DC and FPGA threads to select 1.
   // Round robin arbitration.
   always_comb begin : WRRQ_ARB_IP_ASSIGN
      wrrq_arb_us_id_i[0]    = dc_wr_req_id_i;
      wrrq_arb_us_data_i[0]  = {
			       dc_wr_req_addr_i,
			       dc_wr_req_data_i,
			       dc_wr_req_strb_i
			       };
      wrrq_arb_us_valid_i[0] = dc_wr_req_valid_i;

      wrrq_arb_us_id_i[1]    = ft_wr_req_id_c_i;
      wrrq_arb_us_data_i[1]  = {
			       ft_wr_req_addr_c_i,
			       ft_wr_req_data_c_i,
			       ft_wr_req_strb_c_i
			       };
      wrrq_arb_us_valid_i[1] = ft_wr_req_valid_c_i;
      wrrq_arb_ds_ready_i    = wrrq_ps_us_ready;
   end : WRRQ_ARB_IP_ASSIGN
   axis_comb_rr_arb #
     (
      .ID_WIDTH(MAX_DCU_ID_WIDTH),
      .DATA_WIDTH(WRRQ_ARB_DATA_WIDTH),
      .NUM_IN(2)
       )
   wr_desc_rr_arb_1
     (
      .clk(clk),
      .reset(reset),
      .us_id_i(wrrq_arb_us_id_i),
      .us_data_i(wrrq_arb_us_data_i),
      .us_valid_i(wrrq_arb_us_valid_i),
      .us_ready_o(wrrq_arb_us_ready_o),
      .ds_id_o(wrrq_arb_ds_id_o),
      .ds_data_o(wrrq_arb_ds_data_o),
      .ds_valid_o(wrrq_arb_ds_valid_o),
      .ds_ready_i(wrrq_arb_ds_ready_i)
      );
   // PIpeline register output write request.
   always_comb begin : WRRQ_PS_IP_ASSIGN
      wrrq_ps_us_data  = {wrrq_arb_ds_id_o, wrrq_arb_ds_data_o};
      wrrq_ps_us_valid = wrrq_arb_ds_valid_o;
      wrrq_ps_ds_ready = wr_req_ready_i;
   end : WRRQ_PS_IP_ASSIGN
   axis_pipeline_stage #
     (
      .DATA_WIDTH(WRRQ_PS_DATA_WIDTH)
      )
   wr_req_desc_ps
     (
      .clk(clk),
      .reset(reset),
      .us_data(wrrq_ps_us_data),
      .us_valid(wrrq_ps_us_valid),
      .us_ready(wrrq_ps_us_ready),
      .ds_data(wrrq_ps_ds_data),
      .ds_valid(wrrq_ps_ds_valid),
      .ds_ready(wrrq_ps_ds_ready)
      );

   // Use MSB of ID to route wr rsp to either
   // DC or FPGA threads.
   // if MSB of ID is 1, 0, route to DC else
   // route to FPGA threads.
   always_comb begin : WRRS_RTR_IP_ASSIGN
      wrrs_us_data_i = {wr_rsp_id_i, wr_rsp_bresp_i};
      wrrs_us_sel_down_upbar_i = wr_rsp_id_i[MAX_DCU_ID_WIDTH-1];
      wrrs_us_valid_i = wr_rsp_valid_i;
      wrrs_ds_up_ready_i = dc_wr_rsp_ready_i;
      wrrs_ds_down_ready_i = ft_wr_rsp_ready_i;
   end : WRRS_RTR_IP_ASSIGN
   axis_2_router #
     (
      .DATA_WIDTH(WRRS_RTR_DATA_WIDTH)
      )
   wrrs_rtr_1
     (
      .clk(clk),
      .reset(reset),
      .us_data_i(wrrs_us_data_i),
      // 1 selects down channel; 0 selects up channel. 
      .us_sel_down_upbar_i(wrrs_us_sel_down_upbar_i),
      .us_valid_i(wrrs_us_valid_i),
      .us_ready_o(wrrs_us_ready_o),
      // Up channel sends to DC. 
      .ds_up_data_o(wrrs_ds_up_data_o),
      .ds_up_valid_o(wrrs_ds_up_valid_o),
      .ds_up_ready_i(wrrs_ds_up_ready_i),
      // Down channel sends to FPGA threads.
      .ds_down_data_o(wrrs_ds_down_data_o),
      .ds_down_valid_o(wrrs_ds_down_valid_o),
      .ds_down_ready_i(wrrs_ds_down_ready_i)
      );
   
endmodule // ft_dc_desc_arb

`endif
