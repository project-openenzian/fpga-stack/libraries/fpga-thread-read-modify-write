#!/bin/bash

#DOOM SIM 

xvlog -sv -work worklib \
      $XILINX_VIVADO/data/verilog/src/glbl.v \
      ../rtl/eci_cmd_defs.sv \
      ../rtl/eci_dcs_defs.sv \
      ../testbench/ft_cnc_rmwTb.sv \
      ../rtl/ft_cnc_rmw.sv \
      ../rtl/axis_pipeline_stage.sv \
      ../testbench/word_addr_mem.sv 

xelab -debug typical -incremental -L xpm worklib.ft_cnc_rmwTb worklib.glbl -s worklib.ft_cnc_rmwTb

#Top open GUI replace -R with -gui
# use -view <filename>.wcfg to add additional waveforms 
#xsim -t source_xsim_run.tcl -R worklib.ft_cnc_rmwTb 
xsim -gui worklib.ft_cnc_rmwTb 
