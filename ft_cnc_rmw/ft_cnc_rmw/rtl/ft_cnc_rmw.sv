/*
 * Systems Group, D-INFK, ETH Zurich.
 *
 * Author  : A.Ramdas
 * Date    : 2023-02-23
 * Project : Enzian
 *
 * Copyright (c) 2023, ETH Zurich.  All rights reserved.
 *
 */

`ifndef FT_CNC_RMW_SV
`define FT_CNC_RMW_SV

import eci_cmd_defs::*; // ECI definitions.
import eci_dcs_defs::*; // DCS definitions. 

// IMPORTANT: All addresses input to this module
// are aliased addresses. Dont send unaliased addresses.
// They should be either all odd or all even aliased address. 


// FPGA threads coherent non caching read, modify, write.
// ------------------------------------------
//
// FPGA threads issue a read request with ID and address.
// The module stores the address in a memory indexed by
// the ID and marks this ID as busy (subsequent reads
// to same ID would cause head of line blocking).
//
// The module than issues an LCI to the DC tagged with
// the same ID. When LCIA arrives, the CL is guaratneed
// to be invalid in CPU cache and FPGA memory has most
// up-to-date value. This LCIA also triggers the module
// to read the FPGA memory (mem trnscn tagged with same ID).
//
// When read response arrives, the ID and data
// are sent to the FPGA thread. The thread can now
// modify the data and write it back with the
// same ID. (Note MUST use same ID).
//
// The module then uses the ID from the write
// request to identify the address being written to
// and performs the write request with the mem.
// The same ID is also used to tag the wr trnscn.
//
// when memory responds to the write request,
// the module uses the ID again to identify the
// address and send an unlock message.
// There are no responses to FPGA threads indicating
// the write has completed. 

module ft_cnc_rmw 
  (
   input logic 					clk,
   input logic 					reset,

   // Input read request from FPGA threads.
   // This address is aliased.
   input logic [5-1:0] 				f_rd_req_id_i,
   input logic [DS_ADDR_WIDTH-1:0] 		f_rd_req_al_addr_i,
   input logic 					f_rd_req_valid_i,
   output logic 				f_rd_req_ready_o,

   // Output read response to FPGA threads. 
   output logic [5-1:0] 			f_rd_rsp_id_o,
   output logic [ECI_CL_WIDTH-1:0] 		f_rd_rsp_data_o,
   output logic 				f_rd_rsp_valid_o,
   input logic 					f_rd_rsp_ready_i,

   // Input write by FPGA threads.
   // write only to IDs that have been read before.
   // There is no write strobe, full CLs have to be written.
   // There is no response after write happens. 
   input logic [5-1:0] 				f_wr_id_i,
   input logic [ECI_CL_WIDTH-1:0] 		f_wr_data_i,
   input logic 					f_wr_valid_i,
   output logic 				f_wr_ready_o,

   // Output read request to memory.
   // This address is aliased. 
   output logic [5-1:0] 			rd_req_id_o,
   output logic [DS_ADDR_WIDTH-1:0] 		rd_req_addr_o,
   output logic 				rd_req_valid_o,
   input logic 					rd_req_ready_i,

   // Input read response from memory.
   input logic [5-1:0] 				rd_rsp_id_i,
   input logic [ECI_CL_WIDTH-1:0] 		rd_rsp_data_i,
   input logic 					rd_rsp_valid_i,
   output logic 				rd_rsp_ready_o,

   // Output write request to memory.
   output logic [5-1:0] 			wr_req_id_o,
   output logic [DS_ADDR_WIDTH-1:0] 		wr_req_addr_o,
   output logic [ECI_CL_WIDTH-1:0] 		wr_req_data_o,
   output logic [ECI_CL_SIZE_BYTES-1:0] 	wr_req_strb_o, 
   output logic 				wr_req_valid_o,
   input logic 					wr_req_ready_i,

   // Input write response from memory.
   input logic [5-1:0] 				wr_rsp_id_i,
   input logic [1:0] 				wr_rsp_bresp_i, // not used.
   input logic 					wr_rsp_valid_i,
   output logic 				wr_rsp_ready_o,

   // Output LCI VC 16 or 17
   output logic [ECI_WORD_WIDTH-1:0] 		lcl_fwd_wod_hdr_o,
   output logic [ECI_PACKET_SIZE_WIDTH-1:0] 	lcl_fwd_wod_pkt_size_o,
   output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] lcl_fwd_wod_pkt_vc_o,
   output logic 				lcl_fwd_wod_pkt_valid_o,
   input logic 					lcl_fwd_wod_pkt_ready_i,
   // Input LCIA VC 18 or 19.
   input logic [ECI_WORD_WIDTH-1:0] 		lcl_rsp_wod_hdr_i,
   input logic [ECI_PACKET_SIZE_WIDTH-1:0] 	lcl_rsp_wod_pkt_size_i,
   input logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	lcl_rsp_wod_pkt_vc_i,
   input logic 					lcl_rsp_wod_pkt_valid_i,
   output logic 				lcl_rsp_wod_pkt_ready_o,
   // Output unlock VC 18 or 19.
   output logic [ECI_WORD_WIDTH-1:0] 		lcl_rsp_wod_hdr_o,
   output logic [ECI_PACKET_SIZE_WIDTH-1:0] 	lcl_rsp_wod_pkt_size_o,
   output logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] lcl_rsp_wod_pkt_vc_o,
   output logic 				lcl_rsp_wod_pkt_valid_o,
   input logic 					lcl_rsp_wod_pkt_ready_i
   );

   localparam ID_WIDTH = 5;
   localparam MEM_DEPTH = 2**ID_WIDTH;
   localparam CL_IDX_ODD = 1'b1;
   localparam CL_IDX_EVEN = 1'b0;
   localparam ECI_HDR_PKT_WIDTH = ECI_WORD_WIDTH
				  + ECI_PACKET_SIZE_WIDTH
				  + ECI_LCL_TOT_NUM_VCS_WIDTH;

   typedef struct packed {
      logic [ECI_WORD_WIDTH-1:0]        hdr;
      logic [ECI_PACKET_SIZE_WIDTH-1:0] size;
      logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] vc;
      logic				valid;
      logic				ready;
   } eci_hdr_if_t;

   // new read req.
   logic [ID_WIDTH-1:0] 			n_f_rd_req_id_i;
   logic [DS_ADDR_WIDTH-1:0] 			n_f_rd_req_al_addr_i;
   logic 					n_f_rd_req_valid_i;
   logic 					n_f_rd_req_ready_o;

   // Memory to store CL address corresponding to ID. 
   logic [MEM_DEPTH-1:0][DS_ADDR_WIDTH-1:0] 	id_to_addr_reg;
   logic [ID_WIDTH-1:0] 			i2a_mem_wr_addr_i;
   logic [DS_ADDR_WIDTH-1:0] 			i2a_mem_wr_data_i;
   logic 					i2a_mem_wr_en_i;
   // read interface.
   logic [ID_WIDTH-1:0] 			i2a_mem_rd_addr_ul_i;
   logic [DS_ADDR_WIDTH-1:0] 			i2a_mem_rd_data_ul_o;
   logic [ID_WIDTH-1:0] 			i2a_mem_rd_addr_wr_i;
   logic [DS_ADDR_WIDTH-1:0] 			i2a_mem_rd_data_wr_o;

   // valid filter to indicate which IDs
   // are busy.
   logic [MEM_DEPTH-1:0] 			mem_idx_valid_reg = '0;
   eci_hdr_if_t lci_hdr_ps_i, lci_hdr_ps_o;
   eci_hdr_if_t lcia_hdr_if;
   eci_word_t lcia_hdr;
   // unlock.
   eci_hdr_if_t ul_hdr_ps_i, ul_hdr_ps_o;

   // ID and address from LCIA that will be
   // issued to memory. 
   logic [ID_WIDTH-1:0] 			lcia_rd_req_id_o;
   logic [DS_ADDR_WIDTH-1:0] 			lcia_rd_req_al_addr_o;
   logic 					lcia_rd_req_valid_o;
   logic 					lcia_rd_req_ready_i;

   // read response from memory.
   logic [5-1:0] 				mem_rd_rsp_id_i;
   logic [ECI_CL_WIDTH-1:0] 			mem_rd_rsp_data_i;
   logic 					mem_rd_rsp_valid_i;
   logic 					mem_rd_rsp_ready_o;

   // Unlock
   logic 					unlock_hs;
   

   always_comb begin : OP_ASSIGN
      f_rd_req_ready_o = n_f_rd_req_ready_o & ~mem_idx_valid_reg[f_rd_req_id_i];
      f_rd_rsp_id_o = mem_rd_rsp_id_i;
      f_rd_rsp_data_o = mem_rd_rsp_data_i;
      f_rd_rsp_valid_o = mem_rd_rsp_valid_i;
      // FPGA thread write channel, 
      //  Assigned in Stage 4.
      // Read request to memory.
      //  Assigned in Stage 3.
      // Read response from memory.
      //  Assigned in Stage 3.
      // Write request to memory.
      //  Assigned in Stage 4.
      // Write response
      wr_rsp_ready_o = ul_hdr_ps_i.ready;
      // LCI op channel.
      lcl_fwd_wod_hdr_o       = lci_hdr_ps_o.hdr;
      lcl_fwd_wod_pkt_size_o  = lci_hdr_ps_o.size;
      lcl_fwd_wod_pkt_vc_o    = lci_hdr_ps_o.vc;
      lcl_fwd_wod_pkt_valid_o = lci_hdr_ps_o.valid;
      // LCIA ip channel.
      lcl_rsp_wod_pkt_ready_o = lcia_rd_req_ready_i;
      // Unlock OP channel.
      lcl_rsp_wod_hdr_o       = ul_hdr_ps_o.hdr;
      lcl_rsp_wod_pkt_size_o  = ul_hdr_ps_o.size;
      lcl_rsp_wod_pkt_vc_o    = ul_hdr_ps_o.vc;
      lcl_rsp_wod_pkt_valid_o = ul_hdr_ps_o.valid;
   end : OP_ASSIGN
   
   // Stage 1: 
   // Check if a transaction is already in progress for requested ID.
   // If transaction is in progress for given ID, then stall
   // till previous transaction completes.
   // If not, allow new request to proceed.
   // WARNING: Causes head of line blocking if same IDs are
   // issued back to back.
   always_comb begin : ALIAS_RD_REQ_ADDR
      n_f_rd_req_id_i      = f_rd_req_id_i;
      n_f_rd_req_al_addr_i = f_rd_req_al_addr_i;
      n_f_rd_req_valid_i   = f_rd_req_valid_i & ~mem_idx_valid_reg[f_rd_req_id_i];
   end : ALIAS_RD_REQ_ADDR

   // store  aliased address to be used later for unlocking and
   // mark ID as in progress.
   // The data would be retrieved when unlocking.
   // The data would also be used when issuing write request.
   always_comb begin : I2AMEM_IP_ASSIGN
      i2a_mem_wr_addr_i = n_f_rd_req_id_i;
      i2a_mem_wr_data_i = n_f_rd_req_al_addr_i;
      i2a_mem_wr_en_i   = n_f_rd_req_valid_i & n_f_rd_req_ready_o;
      // one address to read when write response
      // arrives to issue unlock.
      i2a_mem_rd_addr_ul_i = wr_rsp_id_i;
      // another address to read when issuing write request.
      i2a_mem_rd_addr_wr_i = f_wr_id_i;
   end : I2AMEM_IP_ASSIGN
   assign i2a_mem_rd_data_ul_o = id_to_addr_reg[i2a_mem_rd_addr_ul_i];
   assign i2a_mem_rd_data_wr_o = id_to_addr_reg[i2a_mem_rd_addr_wr_i];
   always_ff @(posedge clk) begin : IS_TO_ADDR_MEM
      if(i2a_mem_wr_en_i) begin
	 id_to_addr_reg[i2a_mem_wr_addr_i] <= i2a_mem_wr_data_i;
      end
      if( reset ) begin
	 id_to_addr_reg	<= '0;
      end
   end : IS_TO_ADDR_MEM

   // whenever data gets written into the i2a mem,
   // set the idx bit to valid in the valid filter.
   // whenever unlock is sent, clean up the idx.
   assign unlock_hs = ul_hdr_ps_i.valid & ul_hdr_ps_i.ready;
   always_ff @(posedge clk) begin : VALID_FILTER
      if(i2a_mem_wr_en_i) begin
	 mem_idx_valid_reg[i2a_mem_wr_addr_i] <= 1'b1;
      end
      if(unlock_hs) begin
	mem_idx_valid_reg[i2a_mem_rd_addr_ul_i] <= 1'b0; 
      end
      if( reset ) begin
	 mem_idx_valid_reg <= '0;
      end
   end : VALID_FILTER
   

   // Stage 2:
   //  Generate LCI header and pass it through
   //  a pipeline register before connecting to
   //  output.
   //
   // LCI is tagged with the same ID that is passed
   // in input.
   assign n_f_rd_req_ready_o = lci_hdr_ps_i.ready;
   always_comb begin : GEN_LCI_HDR
      lci_hdr_ps_i.hdr = f_gen_lci_from_al_addr(
						.al_addr_i(n_f_rd_req_al_addr_i),
						// hreq id is 6 bits, rd req is 5 bits.
						.hreq_id_i({1'b0, n_f_rd_req_id_i})
						);
      lci_hdr_ps_i.size = 'd1;
      if(n_f_rd_req_al_addr_i[ECI_CL_ADDR_LSB] == CL_IDX_ODD) begin
	 lci_hdr_ps_i.vc = VC_LCL_FWD_WO_DATA_E; //16
      end else begin
	 lci_hdr_ps_i.vc = VC_LCL_FWD_WO_DATA_O; //17
      end
      lci_hdr_ps_i.valid = n_f_rd_req_valid_i;
   end : GEN_LCI_HDR
   // Register Pipeline LCI before sending to output.
   assign lci_hdr_ps_o.ready = lcl_fwd_wod_pkt_ready_i;
   axis_pipeline_stage #
     (
      .DATA_WIDTH(ECI_HDR_PKT_WIDTH)
       )
   lci_op_pipe_stage
     (
      .clk	(clk),
      .reset	(reset),
      .us_data	({
		  lci_hdr_ps_i.hdr,
		  lci_hdr_ps_i.size,
		  lci_hdr_ps_i.vc
		  }),
      .us_valid	(lci_hdr_ps_i.valid),
      .us_ready	(lci_hdr_ps_i.ready),
      .ds_data	({
		  lci_hdr_ps_o.hdr,
		  lci_hdr_ps_o.size,
		  lci_hdr_ps_o.vc
		  }),
      .ds_valid	(lci_hdr_ps_o.valid),
      .ds_ready	(lci_hdr_ps_o.ready)
      );
   
   // LCIA path.
   // when LCIA arrives, use the ID and address to
   // issue a read request.
   // Registering the outputs.
   always_comb begin : LCIA_RD_REQ
      lcia_hdr              = eci_word_t'(lcl_rsp_wod_hdr_i);
      lcia_rd_req_id_o      = lcia_hdr.lcl_clean_inv_ack.hreq_id[ID_WIDTH-1:0];
      lcia_rd_req_al_addr_o = lcia_hdr.lcl_clean_inv_ack.address[DS_ADDR_WIDTH-1:0];
      lcia_rd_req_valid_o   = lcl_rsp_wod_pkt_valid_i;
   end : LCIA_RD_REQ

   // Add a registered pipeline to output
   // read request. 
   axis_pipeline_stage #
     (
      .DATA_WIDTH(ID_WIDTH+DS_ADDR_WIDTH)
      )
   rd_req_op_pipe_stage
     (
      .clk	(clk),
      .reset	(reset),
      .us_data	({
		  lcia_rd_req_id_o,
		  lcia_rd_req_al_addr_o
		  }),
      .us_valid	(lcia_rd_req_valid_o),
      .us_ready	(lcia_rd_req_ready_i),
      .ds_data	({
		  rd_req_id_o,
		  rd_req_addr_o
		  }),
      .ds_valid	(rd_req_valid_o),
      .ds_ready	(rd_req_ready_i)
      );

   // Stage 3:
   // Read response arrives, send it to the FPGA threads.
   // thread can modify data and write back, it should
   // write from same ID. 
   always_comb begin : MEM_RD_RSP_ASSIGN
      mem_rd_rsp_ready_o = f_rd_rsp_ready_i;
      mem_rd_rsp_id_i    = rd_rsp_id_i;
      mem_rd_rsp_data_i  = rd_rsp_data_i;
      mem_rd_rsp_valid_i = rd_rsp_valid_i;
      rd_rsp_ready_o     = mem_rd_rsp_ready_o;
   end : MEM_RD_RSP_ASSIGN


   // Stage 4:
   // Use ID from write request to read the
   // original address. 
   always_comb begin : WR_REQ_TO_MEM_ASSIGN
      wr_req_id_o    = f_wr_id_i;
      wr_req_addr_o  = i2a_mem_rd_data_wr_o;
      wr_req_data_o  = f_wr_data_i;
      wr_req_strb_o  = '1;
      wr_req_valid_o = f_wr_valid_i;
      f_wr_ready_o   = wr_req_ready_i;
   end : WR_REQ_TO_MEM_ASSIGN

   // Stage 5:
   // When memory responds, use ID to look up
   // address to unlock and gen unlock message.
   // Pass unlock message through registered pipeline
   // stage before sending to output. 
   always_comb begin : GEN_UNLOCK
      ul_hdr_ps_i.hdr = gen_ul_from_al_addr(.al_addr_i(i2a_mem_rd_data_ul_o));
      ul_hdr_ps_i.size = 'd1;
      if(ul_hdr_ps_i.hdr[ECI_CL_ADDR_LSB] == CL_IDX_ODD) begin
	 ul_hdr_ps_i.vc = VC_LCL_RESP_WO_DATA_E; //18
      end else begin
	 ul_hdr_ps_i.vc = VC_LCL_RESP_WO_DATA_O; //19
      end
      ul_hdr_ps_i.valid = wr_rsp_valid_i;      
   end : GEN_UNLOCK
   // Registered pipeline stage for output.
   assign ul_hdr_ps_o.ready = lcl_rsp_wod_pkt_ready_i;
   axis_pipeline_stage #
     (
      .DATA_WIDTH(ECI_HDR_PKT_WIDTH)
       )
   ul_op_pipe_stage
     (
      .clk	(clk),
      .reset	(reset),
      .us_data	({
		  ul_hdr_ps_i.hdr,
		  ul_hdr_ps_i.size,
		  ul_hdr_ps_i.vc
		  }),
      .us_valid	(ul_hdr_ps_i.valid),
      .us_ready	(ul_hdr_ps_i.ready),
      .ds_data	({
		  ul_hdr_ps_o.hdr,
		  ul_hdr_ps_o.size,
		  ul_hdr_ps_o.vc
		  }),
      .ds_valid	(ul_hdr_ps_o.valid),
      .ds_ready	(ul_hdr_ps_o.ready)
      );

   // -- Functions below ---//
   function automatic [ECI_WORD_WIDTH-1:0] f_gen_lci_from_al_addr
     (
      input logic [DS_ADDR_WIDTH-1:0] al_addr_i,
      input logic [ECI_HREQID_WIDTH-1:0] hreq_id_i // 6 bits. 
      );
      // Generate and return LCI header
      // from input aliased address.
      eci_word_t eci_cmd;
      eci_cmd.eci_word = '0;
      eci_cmd.lcl_clean_inv.opcode = LCL_CMD_MFWD_CLEAN_INV;
      eci_cmd.lcl_clean_inv.hreq_id = hreq_id_i;
      eci_cmd.lcl_clean_inv.dmask = '1;
      eci_cmd.lcl_clean_inv.ns = 1'b1;
      eci_cmd.lcl_clean_inv.rnode = eci_nodeid_t'(ECI_FPGA_NODE_ID);
      eci_cmd.lcl_clean_inv.address[DS_ADDR_WIDTH-1:0] = al_addr_i;
      return(eci_cmd.eci_word);
   endfunction : f_gen_lci_from_al_addr

   function automatic [ECI_WORD_WIDTH-1:0] gen_ul_from_al_addr
     (
      input logic [DS_ADDR_WIDTH-1:0] al_addr_i
      );
      // Generate Unlock message given aliased address.
      eci_word_t ul_hdr_c;
      ul_hdr_c.eci_word = '0;
      ul_hdr_c.lcl_unlock.opcode = LCL_CMD_MRSP_UNLOCK;
      ul_hdr_c.lcl_unlock.address[DS_ADDR_WIDTH-1:0] = al_addr_i;
      return(ul_hdr_c.eci_word);
   endfunction : gen_ul_from_al_addr
   
endmodule // ft_cnc_rmw

`endif
