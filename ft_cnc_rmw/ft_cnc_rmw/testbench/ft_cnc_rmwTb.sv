import eci_cmd_defs::*; 
import eci_dcs_defs::*; 

module ft_cnc_rmwTb();

   localparam CL_IDX_ODD = 1'b1;
   localparam CL_IDX_EVEN = 1'b0;

   //input output ports 
   //Input signals
   logic 					clk;
   logic 					reset;
   logic [5-1:0] 				f_rd_req_id_i;
   logic [DS_ADDR_WIDTH-1:0] 			f_rd_req_al_addr_i;
   logic 					f_rd_req_valid_i;
   logic 					f_rd_rsp_ready_i;
   logic [5-1:0] 				f_wr_id_i;
   logic [ECI_CL_WIDTH-1:0] 			f_wr_data_i;
   logic 					f_wr_valid_i;
   logic 					rd_req_ready_i;
   logic [5-1:0] 				rd_rsp_id_i;
   logic [ECI_CL_WIDTH-1:0] 			rd_rsp_data_i;
   logic 					rd_rsp_valid_i;
   logic 					wr_req_ready_i;
   logic [5-1:0] 				wr_rsp_id_i;
   logic [1:0] 					wr_rsp_bresp_i;
   logic 					wr_rsp_valid_i;
   logic 					lcl_fwd_wod_pkt_ready_i;
   logic [ECI_WORD_WIDTH-1:0] 			lcl_rsp_wod_hdr_i;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 		lcl_rsp_wod_pkt_size_i;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	lcl_rsp_wod_pkt_vc_i;
   logic 					lcl_rsp_wod_pkt_valid_i;
   logic 					lcl_rsp_wod_pkt_ready_i;

   //Output signals
   logic 					f_rd_req_ready_o;
   logic [5-1:0] 				f_rd_rsp_id_o;
   logic [ECI_CL_WIDTH-1:0] 			f_rd_rsp_data_o;
   logic 					f_rd_rsp_valid_o;
   logic 					f_wr_ready_o;
   logic [5-1:0] 				rd_req_id_o;
   logic [DS_ADDR_WIDTH-1:0] 			rd_req_addr_o;
   logic 					rd_req_valid_o;
   logic 					rd_rsp_ready_o;
   logic [5-1:0] 				wr_req_id_o;
   logic [DS_ADDR_WIDTH-1:0] 			wr_req_addr_o;
   logic [ECI_CL_WIDTH-1:0] 			wr_req_data_o;
   logic [ECI_CL_SIZE_BYTES-1:0] 		wr_req_strb_o;
   logic 					wr_req_valid_o;
   logic 					wr_rsp_ready_o;
   logic [ECI_WORD_WIDTH-1:0] 			lcl_fwd_wod_hdr_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 		lcl_fwd_wod_pkt_size_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	lcl_fwd_wod_pkt_vc_o;
   logic 					lcl_fwd_wod_pkt_valid_o;
   logic 					lcl_rsp_wod_pkt_ready_o;
   logic [ECI_WORD_WIDTH-1:0] 			lcl_rsp_wod_hdr_o;
   logic [ECI_PACKET_SIZE_WIDTH-1:0] 		lcl_rsp_wod_pkt_size_o;
   logic [ECI_LCL_TOT_NUM_VCS_WIDTH-1:0] 	lcl_rsp_wod_pkt_vc_o;
   logic 					lcl_rsp_wod_pkt_valid_o;

   //Clock block comment if not needed
   always #10 clk =~ clk;
   default clocking cb @(posedge clk);
   endclocking

   //instantiation
   ft_cnc_rmw ft_cnc_rmw1 (
			   .clk(clk),
			   .reset(reset),
			   .f_rd_req_id_i(f_rd_req_id_i),
			   .f_rd_req_al_addr_i(f_rd_req_al_addr_i),
			   .f_rd_req_valid_i(f_rd_req_valid_i),
			   .f_rd_req_ready_o(f_rd_req_ready_o),

			   .f_rd_rsp_id_o(f_rd_rsp_id_o),
			   .f_rd_rsp_data_o(f_rd_rsp_data_o),
			   .f_rd_rsp_valid_o(f_rd_rsp_valid_o),
			   .f_rd_rsp_ready_i(f_rd_rsp_ready_i),

			   .f_wr_id_i(f_wr_id_i),
			   .f_wr_data_i(f_wr_data_i),
			   .f_wr_valid_i(f_wr_valid_i),
			   .f_wr_ready_o(f_wr_ready_o),

			   .rd_req_id_o(rd_req_id_o),
			   .rd_req_addr_o(rd_req_addr_o),
			   .rd_req_valid_o(rd_req_valid_o),
			   .rd_req_ready_i(rd_req_ready_i),
			   
			   .rd_rsp_id_i(rd_rsp_id_i),
			   .rd_rsp_data_i(rd_rsp_data_i),
			   .rd_rsp_valid_i(rd_rsp_valid_i),
			   .rd_rsp_ready_o(rd_rsp_ready_o),

			   .wr_req_id_o(wr_req_id_o),
			   .wr_req_addr_o(wr_req_addr_o),
			   .wr_req_data_o(wr_req_data_o),
			   .wr_req_strb_o(wr_req_strb_o),
			   .wr_req_valid_o(wr_req_valid_o),
			   .wr_req_ready_i(wr_req_ready_i),
			   
			   .wr_rsp_id_i(wr_rsp_id_i),
			   .wr_rsp_bresp_i(wr_rsp_bresp_i),
			   .wr_rsp_valid_i(wr_rsp_valid_i),
			   .wr_rsp_ready_o(wr_rsp_ready_o),
			   // lci 
			   .lcl_fwd_wod_hdr_o(lcl_fwd_wod_hdr_o),
			   .lcl_fwd_wod_pkt_size_o(lcl_fwd_wod_pkt_size_o),
			   .lcl_fwd_wod_pkt_vc_o(lcl_fwd_wod_pkt_vc_o),
			   .lcl_fwd_wod_pkt_valid_o(lcl_fwd_wod_pkt_valid_o),
			   .lcl_fwd_wod_pkt_ready_i(lcl_fwd_wod_pkt_ready_i),
			   // lcia 
			   .lcl_rsp_wod_hdr_i(lcl_rsp_wod_hdr_i),
			   .lcl_rsp_wod_pkt_size_i(lcl_rsp_wod_pkt_size_i),
			   .lcl_rsp_wod_pkt_vc_i(lcl_rsp_wod_pkt_vc_i),
			   .lcl_rsp_wod_pkt_valid_i(lcl_rsp_wod_pkt_valid_i),
			   .lcl_rsp_wod_pkt_ready_o(lcl_rsp_wod_pkt_ready_o),
			   // unlock
			   .lcl_rsp_wod_hdr_o(lcl_rsp_wod_hdr_o),
			   .lcl_rsp_wod_pkt_size_o(lcl_rsp_wod_pkt_size_o),
			   .lcl_rsp_wod_pkt_vc_o(lcl_rsp_wod_pkt_vc_o),
			   .lcl_rsp_wod_pkt_valid_o(lcl_rsp_wod_pkt_valid_o),
			   .lcl_rsp_wod_pkt_ready_i(lcl_rsp_wod_pkt_ready_i)
			   );//instantiation completed 

   initial begin
      //$dumpfile("test.vcd");
      //$dumpvars();

      // Initialize Input Ports 
      clk = '0;
      reset = '1;
      f_rd_req_id_i = '0;
      f_rd_req_al_addr_i = '0;
      f_rd_req_valid_i = '0;
      //f_rd_rsp_ready_i = '0;
      //f_wr_id_i = '0;
      //f_wr_data_i = '0;
      //f_wr_valid_i = '0;
      //rd_req_ready_i = '0;
      //rd_rsp_id_i = '0;
      //rd_rsp_data_i = '0;
      //rd_rsp_valid_i = '0;
      //wr_req_ready_i = '0;
      //wr_rsp_id_i = '0;
      //wr_rsp_bresp_i = '0;
      //wr_rsp_valid_i = '0;
      //lcl_fwd_wod_pkt_ready_i = '0;
      //lcl_rsp_wod_hdr_i = '0;
      //lcl_rsp_wod_pkt_size_i = '0;
      //lcl_rsp_wod_pkt_vc_i = '0;
      //lcl_rsp_wod_pkt_valid_i = '0;
      lcl_rsp_wod_pkt_ready_i = '0;

   
      ##5;
      reset = 1'b0;
      lcl_rsp_wod_pkt_ready_i = '1; 
      // issue read request,
      // wait for response.
      // response arrives data gets written back.
      // ft_rd(.id_i('d1), .addr_i('d128));
      // wait(f_rd_rsp_valid_o & f_rd_rsp_ready_i);
      // ##1;
      // ft_rd(.id_i('d1), .addr_i('d128));
      // wait(f_rd_rsp_valid_o & f_rd_rsp_ready_i);
      // ##1;
      // ##10;

      for(integer i=0; i<10; i=i+1) begin
	 ft_rd(.id_i(i), .addr_i(i*128));
      end
      
      #500 $finish;
   end // initial begin

   // LCI LCIA loopback.
   always_comb begin : LCI_LCIA_LOOPBACK
      lcl_rsp_wod_hdr_i = eci_cmd_defs::lcl_gen_lcia(lcl_fwd_wod_hdr_o);
      lcl_rsp_wod_pkt_size_i = 'd1;
      if(lcl_rsp_wod_hdr_i[ECI_CL_ADDR_LSB] == CL_IDX_ODD) begin
	 lcl_rsp_wod_pkt_vc_i = VC_LCL_RESP_WO_DATA_E; //18
      end else begin
	 lcl_rsp_wod_pkt_vc_i = VC_LCL_RESP_WO_DATA_O; //19
      end
      lcl_rsp_wod_pkt_valid_i = lcl_fwd_wod_pkt_valid_o;
      lcl_fwd_wod_pkt_ready_i = lcl_rsp_wod_pkt_ready_o;
   end : LCI_LCIA_LOOPBACK

   // Modify the data and write it back.
   always_comb begin : MODIFY_WRITE
      f_wr_id_i = f_rd_rsp_id_o;
      f_wr_data_i = f_rd_rsp_data_o + 'd100;
      f_wr_valid_i = f_rd_rsp_valid_o;
      f_rd_rsp_ready_i = f_wr_ready_o;
   end : MODIFY_WRITE

   // This is word addressable memory.
   // An address points to a CL.
   // Give CL index as input to this module.
   // It can hold 2^10 CLs. (only for sim).
   word_addr_mem #
     (
      .ID_WIDTH(5),
      .ADDR_WIDTH(10),
      .DATA_WIDTH(ECI_CL_WIDTH)
       )
   mem_inst_1
     (
      .clk(clk),
      .reset(reset),

      .rd_req_id_i(rd_req_id_o),
      .rd_req_addr_i(rd_req_addr_o[9+7:7]),
      .rd_req_valid_i(rd_req_valid_o),
      .rd_req_ready_o(rd_req_ready_i),

      .wr_req_id_i(wr_req_id_o),
      .wr_req_addr_i(wr_req_addr_o[9+7:7]),
      .wr_req_data_i(wr_req_data_o),
      .wr_req_valid_i(wr_req_valid_o),
      .wr_req_ready_o(wr_req_ready_i),

      .rd_rsp_id_o(rd_rsp_id_i),
      .rd_rsp_data_o(rd_rsp_data_i),
      .rd_rsp_valid_o(rd_rsp_valid_i),
      .rd_rsp_ready_i(rd_rsp_ready_o),

      .wr_rsp_id_o(wr_rsp_id_i),
      .wr_rsp_bresp_o(wr_rsp_bresp_i),
      .wr_rsp_valid_o(wr_rsp_valid_i),
      .wr_rsp_ready_i(wr_rsp_ready_o)
      );


   
   task static ft_rd(
		     input logic [5-1:0] 	     id_i,
		     input logic [DS_ADDR_WIDTH-1:0] addr_i
		     );
      // FPGA thread read request.
      f_rd_req_id_i = id_i;
      f_rd_req_al_addr_i = addr_i;
      f_rd_req_valid_i = 1'b1;
      wait(f_rd_req_valid_i & f_rd_req_ready_o);
      ##1;
      f_rd_req_valid_i = 1'b0;
      #1;
   endtask //ft_rd
endmodule
